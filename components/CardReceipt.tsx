import React from 'react';
import { StyleSheet, Image, TouchableOpacity } from 'react-native';
import Layout from '../constants/Layout';
import { TextInverted, View } from './Themed';
import useColorScheme from '../hooks/useColorScheme';
import Colors from '../constants/Colors';
import { useNavigation } from '@react-navigation/native';

interface CardReceiptProps {
    name: string;
    ingredients: Array<string>;
    preparation: Array<string> | undefined;
    image: string;
    description: string;
    difficulty: string;
    time: string;
    by: string;
    video?: string;
};

export default function CardReceipt(props: CardReceiptProps) {
    const scheme = useColorScheme();
    const navigation = useNavigation();

    return (
        <TouchableOpacity onPress={() => navigation.navigate('FullReceiptScreen', { props })} style={[styles.container, { backgroundColor: scheme === "dark" ? Colors.dark.tint : Colors.light.tint }]}>
            <View style={styles.centered}>
                <Image source={{ uri: props.image }} style={styles.image} resizeMode={"cover"} />
            </View>
            <TextInverted style={styles.title}>{props.name}</TextInverted>
            <TextInverted style={styles.subTitle}>{props.description}</TextInverted>
            <TextInverted style={styles.subTitleBold}>{"Temps: " + props.time}</TextInverted>
            <TextInverted style={styles.subTitleBold}>{"Difficulté: " + props.difficulty}</TextInverted>
            <TextInverted style={styles.subTitleBy}>{"par " + props.by}</TextInverted>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        width: Layout.window.width * 0.8 - 40,
        margin: 20,
        borderRadius: 25,
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.41,
        shadowRadius: 9.11,
        elevation: 14,
    },
    centered: {
        alignItems: 'center',
        width: '100%',
        height: (Layout.window.width * 0.8 - 40) / (16 / 9),
        borderRadius: 25,
    },
    image: {
        width: '100%',
        height: '100%',
        borderRadius: 25
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        fontFamily: 'bold',
        paddingLeft: 10,
        paddingTop: 10
    },
    subTitle: {
        fontSize: 15,
        fontFamily: 'light',
        paddingHorizontal: 10,
        paddingTop: 3,
        textAlign: 'justify'
    },
    subTitleBold: {
        fontSize: 17,
        fontFamily: 'bold',
        paddingLeft: 10,
        paddingTop: 10
    },
    subTitleBy: {
        textAlign: 'right',
        fontSize: 14,
        fontFamily: 'light',
        paddingRight: 10,
        paddingTop: 20,
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
});
