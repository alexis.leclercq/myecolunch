import React, { useState } from 'react';
import { StyleSheet, View as DefaultView, TouchableOpacity } from 'react-native';
import { TextInverted } from './Themed';
import useColorScheme from '../hooks/useColorScheme';
import Colors from '../constants/Colors';

export default function CartElement(props: { name: string, quantity: string, price: number, setShop: any, number?: number, canEdit: boolean }) {
    const scheme = useColorScheme();

    const [number, setNumber] = useState<number>(props.number ? props.number : 0);

    const addOne = () => {
        if (number < 15 && number >= 0) {
            let shouldUpdate = props.setShop(props.name, props.quantity, number + 1, props.price, "plus");
            if (shouldUpdate)
                setNumber(number + 1);
        }
    }

    const removeOne = () => {
        if (number <= 15 && number > 0) {
            setNumber(number - 1);
            props.setShop(props.name, props.quantity, number - 1, props.price, "less");
        }
    }

    return (
        <DefaultView style={[styles.container, { backgroundColor: Colors[scheme].tint }]}>
            <DefaultView style={styles.startView}>
                <TextInverted style={styles.title}>{props.name}</TextInverted>
                <TextInverted style={styles.subTitle}>{props.price + "€ / " + props.quantity}</TextInverted>
            </DefaultView>
            <DefaultView style={styles.endView}>
                {props.canEdit && <TouchableOpacity onPress={removeOne} style={{ width: 30 }}>
                    <TextInverted style={styles.bigTitle}>{"-"}</TextInverted>
                </TouchableOpacity>}
                <TextInverted style={props.canEdit ? styles.bigTitleN : styles.bigTitleCantN}>{number}</TextInverted>
                {props.canEdit && <TouchableOpacity onPress={addOne}>
                    <TextInverted style={styles.bigTitle}>{"+"}</TextInverted>
                </TouchableOpacity>}
            </DefaultView>
        </DefaultView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        paddingLeft: 15,
        minHeight: 50,
        marginVertical: 5,
        borderRadius: 25,
        flexDirection: 'row',
        alignItems: 'center',
    },
    startView: {
        width: '70%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    endView: {
        width: '30%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    centered: {
        alignItems: 'center',
        width: '100%',
        height: '30%',
        borderRadius: 25,
    },
    image: {
        width: '100%',
        height: '100%',
        borderRadius: 25
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: 'bold',
        textAlign: 'left',
        color: Colors.dark.text
    },
    bigTitle: {
        fontSize: 30,
        fontWeight: 'bold',
        fontFamily: 'bold',
        textAlign: 'left',
        marginRight: 7,
        color: Colors.dark.text
    },
    bigTitleN: {
        width: 40,
        fontSize: 30,
        fontWeight: 'bold',
        fontFamily: 'bold',
        textAlign: 'left',
        marginRight: 7,
        color: Colors.dark.text
    },
    bigTitleCantN: {
        width: 40,
        fontSize: 30,
        fontWeight: 'bold',
        fontFamily: 'bold',
        textAlign: 'right',
        color: Colors.dark.text
    },
    subTitle: {
        fontSize: 13,
        fontFamily: 'light',
        paddingLeft: 10,
        paddingTop: 3,
        color: Colors.dark.text
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
});
