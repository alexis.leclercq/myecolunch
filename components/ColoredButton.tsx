import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';

import Colors from '../constants/Colors';

export default class ColoredButton extends Component<{ text: any, style?: any, onPress: any }> {
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} style={[styles.button, this.props.style]}>
                <Text style={styles.text} adjustsFontSizeToFit>{this.props.text}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: Colors.light.tint,
        borderRadius: 10,
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 0,
        // },
        // shadowOpacity: 0.41,
        // shadowRadius: 9.11,
        // elevation: 14,
        paddingVertical: 7,
        paddingHorizontal: 10
    },
    text: {
        color: Colors.dark.text,
        fontFamily: 'bold',
        fontSize: 22,
        textAlign: 'center',
    }
});
