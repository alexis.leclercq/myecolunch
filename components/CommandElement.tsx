import React from 'react';
import { StyleSheet, View as DefaultView, TouchableOpacity, Platform } from 'react-native';
import { TextInverted } from './Themed';
import useColorScheme from '../hooks/useColorScheme';
import Colors from '../constants/Colors';
import { ICommand } from '../types';
import moment from 'moment';
import 'moment/locale/fr';
import CommandStatus from '../constants/CommandStatus';
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

export default function CommandElement(props: ICommand) {
    const scheme = useColorScheme();
    const navigation = useNavigation();

    return (
        <TouchableOpacity onPress={() => navigation.navigate('ManageCommandScreen', { props })}>
            <DefaultView style={[styles.container, { backgroundColor: Colors[scheme].tint }]}>
                <DefaultView style={styles.row}>
                    <TextInverted style={styles.title}>{props.name}</TextInverted>
                    <TextInverted style={styles.subTitle}>{Number((props.amount).toFixed(2)) + "€"}</TextInverted>
                </DefaultView>
                <DefaultView style={styles.row}>
                    <DefaultView>
                        <TextInverted style={[styles.title, { marginTop: 20 }]}>{CommandStatus[props.status].title}</TextInverted>
                        <TextInverted style={styles.subTitle}>{moment(props.date).calendar()}</TextInverted>
                    </DefaultView>
                    <Ionicons size={35} style={{ marginBottom: -3, marginRight: 10 }} name={Platform.OS === "android" ? "md-arrow-forward" : "ios-arrow-forward"} color={Colors.dark.text} />
                </DefaultView>
            </DefaultView>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        paddingLeft: 15,
        marginVertical: 5,
        borderRadius: 25,
    },
    startView: {
        width: '70%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    endView: {
        width: '30%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    centered: {
        alignItems: 'center',
        width: '100%',
        height: '30%',
        borderRadius: 25,
    },
    image: {
        width: '100%',
        height: '100%',
        borderRadius: 25
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: 'bold',
        textAlign: 'left',
        color: Colors.dark.text
    },
    bigTitle: {
        fontSize: 30,
        fontWeight: 'bold',
        fontFamily: 'bold',
        textAlign: 'left',
        marginRight: 7,
        color: Colors.dark.text
    },
    bigTitleN: {
        width: 40,
        fontSize: 30,
        fontWeight: 'bold',
        fontFamily: 'bold',
        textAlign: 'left',
        marginRight: 7,
        color: Colors.dark.text
    },
    bigTitleCantN: {
        width: 40,
        fontSize: 30,
        fontWeight: 'bold',
        fontFamily: 'bold',
        textAlign: 'right',
        color: Colors.dark.text
    },
    subTitle: {
        fontSize: 13,
        fontFamily: 'light',
        paddingTop: 3,
        color: Colors.dark.text
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
});
