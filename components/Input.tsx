import React, { Component } from 'react';
import { ReturnKeyTypeOptions, StyleSheet, TextInput, View } from 'react-native';
import { ViewInverted } from '../components/Themed';
import Colors from '../constants/Colors';

interface IInputContainer {
    style?: any,
    endComponent?: any,
    onRef?: ((t: any) => void),
    placeholder?: string,
    dark: boolean,
    search?: boolean,
    returnKeyType?: ReturnKeyTypeOptions,
    maxLength?: number,
    onChangeText?: ((text: string) => void) | undefined,
    onSubmitEditing?: ((e: any) => void) | undefined,
    value?: string | undefined,
    autoCapitalize?: "none" | "sentences" | "words" | "characters" | undefined,
    keyboardType?: "default" | "email-address" | "numeric" | "phone-pad" | "number-pad" | "decimal-pad" | "visible-password" | "ascii-capable" | "numbers-and-punctuation" | "url" | "name-phone-pad" | "twitter" | "web-search" | undefined,
    secureTextEntry?: boolean | undefined,
    autoCorrect?: boolean | undefined,
    autoCompleteType?: "name" | "username" | "password" | "cc-csc" | "cc-exp" | "cc-exp-month" | "cc-exp-year" | "cc-number" | "email" | "postal-code" | "street-address" | "tel" | "off" | undefined
}

export default class Input extends Component<IInputContainer> {

    textInput: TextInput | null = null;

    componentDidMount() {
        if (this.props.onRef)
            this.props.onRef(this);
    }

    componentDidUpdate() {
        if (this.props.onRef)
            this.props.onRef(this);
    }

    focus() {
        this.textInput?.focus();
    }

    render() {
        return (
            <ViewInverted style={[styles.button, this.props.style, { backgroundColor: this.props.search && !this.props.dark ? '#fff' : this.props.dark ? Colors.dark.backgroundInverted : Colors.light.backgroundInverted }]}>
                <TextInput
                    placeholder={this.props.placeholder}
                    ref={input => this.textInput = input}
                    maxLength={this.props.maxLength}
                    onSubmitEditing={this.props.onSubmitEditing}
                    onChangeText={this.props.onChangeText}
                    value={this.props.value}
                    autoCorrect={this.props.autoCorrect}
                    autoCompleteType={this.props.autoCompleteType}
                    autoCapitalize={this.props.autoCapitalize}
                    keyboardType={this.props.keyboardType}
                    secureTextEntry={this.props.secureTextEntry}
                    returnKeyType={this.props.returnKeyType}
                    style={[styles.text, { color: this.props.dark ? Colors.dark.text : Colors.light.text }]}
                />
                {this.props.endComponent && this.props.endComponent()}
            </ViewInverted>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        borderRadius: 10,
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 0,
        // },
        // shadowOpacity: 0.41,
        // shadowRadius: 9.11,
        // elevation: 14,
        paddingVertical: 7,
        paddingHorizontal: 10,
        flexDirection: 'row',
    },
    text: {
        fontFamily: 'light',
        fontSize: 18,
        textAlign: 'left',
        width: '90%'
    }
});
