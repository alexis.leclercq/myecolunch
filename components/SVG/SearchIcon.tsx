import * as React from "react"
import Svg, { Rect, Ellipse } from "react-native-svg"

function SearchIcon(props: any) {
  return (
    <Svg
      width={22}
      height={25}
      viewBox="0 0 22 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Rect
        width={11.25}
        height={3.572}
        rx={1.786}
        transform="matrix(.65172 -.75846 .75092 .66039 0 22.28)"
        fill="#C4C4C4"
      />
      <Ellipse cx={13.115} cy={9.033} rx={8.885} ry={9.033} fill="#C4C4C4" />
      <Ellipse cx={13.115} cy={9.033} rx={6.346} ry={6.452} fill={props.color} />
    </Svg>
  )
}

export default SearchIcon
