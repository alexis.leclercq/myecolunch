import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { TouchableOpacity, Text } from '../components/Themed';

export default class SimpleButton extends Component<{ text: any, style?: any, onPress: any }> {
    
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} style={[styles.button, this.props.style]}>
                <Text style={styles.text} adjustsFontSizeToFit>{this.props.text}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        borderRadius: 10,
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 0,
        // },
        // shadowOpacity: 0.41,
        // shadowRadius: 9.11,
        // elevation: 14,
        paddingVertical: 7,
        paddingHorizontal: 10
    },
    text: {
        fontFamily: 'bold',
        fontSize: 22,
        textAlign: 'center',
    }
});
