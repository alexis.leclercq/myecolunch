import React from "react";
import { StyleSheet, View, Animated, Platform } from "react-native";
import MaskedView from '@react-native-community/masked-view';

export default class SplashAnimation extends React.Component {

    state = {
        loadingProgress: new Animated.Value(0),
        animationDone: false
    };

    componentDidMount() {
        Animated.timing(this.state.loadingProgress, {
            toValue: 100,
            duration: 1000,
            useNativeDriver: true,
            delay: 400
        }).start(() => {
            this.setState({ animationDone: true });
        })
    }

    render() {
        const colorLayer = !this.state.animationDone && <View style={[StyleSheet.absoluteFill, { backgroundColor: "#295828" }]} />;
        const whiteLayer = !this.state.animationDone && <View style={[StyleSheet.absoluteFill, { backgroundColor: "#FFF" }]} />;

        const imageScale = {
            transform: [
                {
                    scale: this.state.loadingProgress.interpolate({
                        inputRange: [0, 15, 100],
                        outputRange: [0.1, 0.06, 16]
                    })
                }
            ]
        }

        const opacity = {
            opacity: this.state.loadingProgress.interpolate({
                inputRange: [0, 25, 50],
                outputRange: [0, 0, 1],
                extrapolate: "clamp"
            })
        }

        return (
            Platform.OS === "ios" ? (
                <View style={{ flex: 1 }}>
                    {colorLayer}
                    <MaskedView style={{ flex: 1 }} maskElement={
                        <View style={styles.centered}>
                            <Animated.Image
                                source={require('../assets/images/carot.png')}
                                style={[{ width: 1000 }, imageScale]}
                                resizeMode="contain"
                            />
                        </View>
                    }>
                        {whiteLayer}
                        <Animated.View style={[styles.centered, opacity, { backgroundColor: 'red' }]}>
                            {this.props.children}
                        </Animated.View>
                    </MaskedView>
                </View>
            ) : (
                    <View style={{ flex: 1 }}>
                        {this.props.children}
                    </View>
                )
        );
    }
}

const styles = StyleSheet.create({
    centered: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    }
});