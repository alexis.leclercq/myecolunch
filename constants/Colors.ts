const tintColorLight = '#295828';
const tintColorDark = '#295828';

export default {
  light: {
    text: '#000',
    textInverted: '#fff',
    background: '#fff',
    backgroundInverted: '#efefef',
    tint: tintColorLight,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
  },
  dark: {
    text: '#fff',
    textInverted: '#fff',
    background: '#1f1f1f',
    backgroundInverted: '#4a4a4a',
    tint: tintColorDark,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
  },
};
