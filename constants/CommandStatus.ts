const status: any = {
    send: {
        title: "Récupérée",
    },
    sendAsked: {
        title: "Attente de stock",
    },
    sendWait: {
        title: "Prête",
    },
    pending: {
        title: "En attente",
    },
    canceled: {
        title: "Annulée",
    },
};

export default status;