import * as firebase from 'firebase';
import { Platform } from 'react-native';

// Optionally import the services that you want to use
import "firebase/auth";
//import "firebase/database";
import "firebase/firestore";
//import "firebase/functions";
//import "firebase/storage";

// Initialize Firebase
const firebaseConfig = {
  apiKey: Platform.OS === "android" ? "AIzaSyBSRP9zCJ3BMp7_4e-3MektQcWeagpbcfg" : "AIzaSyC60h_6avofVGe1MIRv7zxUAE9edjaV0l4",
  authDomain: "project-id.firebaseapp.com",
  databaseURL: "https://my-eco-lunch.firebaseio.com",
  projectId: "my-eco-lunch",
  storageBucket: "my-eco-lunch.appspot.com",
  messagingSenderId: "424756881956",
  appId: Platform.OS === "android" ? "1:424756881956:android:123a58d34cd729569bc6b1" : "1:424756881956:ios:d27ee1ad884209b59bc6b1",
  measurementId: "G-measurement-id"
};

firebase.initializeApp(firebaseConfig);

export default firebase;