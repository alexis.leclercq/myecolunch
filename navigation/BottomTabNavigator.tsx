import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { Platform } from 'react-native';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import { BottomTabParamList, NotFoundParamList, MapParamList, CartParamList, ReceiptParamList, ProfileParamList, SettingsParamList } from '../types';
import NotFoundScreen from '../screens/NotFoundScreen';
import SettingsScreen from '../screens/Settings/SettingsScreen';
import ProfileScreen from '../screens/Profile/ProfileScreen';
import MapScreen from '../screens/Map/MapScreen';
import CartScreen from '../screens/Cart/CartScreen';
import ReceiptScreen from '../screens/Receipt/ReceiptScreen';
import CommandScreen from '../screens/Cart/CommandScreen';
import CommandMaintainScreen from '../screens/Settings/CommandMaintainScreen';
import ManageCommandScreen from '../screens/Settings/ManageCommandScreen';
import ScannerScreen from '../screens/Profile/ScannerScreen';
import StatsPartnerScreen from '../screens/Settings/StatsPartnerScreen';
import FullReceiptScreen from '../screens/Receipt/FullReceiptScreen';
import PartnerScreen from '../screens/Map/PartnerScreen';
import StatsByUserScreen from '../screens/Settings/StatsByUserScreen';
import StatsByPartnerScreen from '../screens/Settings/StatsByPartnerScreen';
import ListPartnerScanScreen from '../screens/Settings/ListPartnerScanScreen';
import ListUserScanScreen from '../screens/Settings/ListUserScanScreen';
import OnboardingScreen from '../screens/Settings/OnboardingScreen';
import InfoScreen from '../screens/Profile/InfoScreen';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="MapScreen"
      tabBarOptions={{ activeTintColor: Colors[colorScheme].tint, showLabel: false }}>
      <BottomTab.Screen
        name="MapScreen"
        component={MapNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name={Platform.OS === "android" ? "md-pin" : "ios-pin"} color={color} />,
        }}
      />
      <BottomTab.Screen
        name="CartScreen"
        component={CartNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name={Platform.OS === "android" ? "md-cart" : "ios-cart"} color={color} />,
        }}
      />
      <BottomTab.Screen
        name="ReceiptScreen"
        component={ReceiptNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name={Platform.OS === "android" ? "md-restaurant" : "ios-restaurant"} color={color} />,
        }}
      />
      <BottomTab.Screen
        name="ProfileScreen"
        component={ProfileNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name={Platform.OS === "android" ? "md-person" : "ios-person"} color={color} />,
        }}
      />
      <BottomTab.Screen
        name="SettingsScreen"
        component={SettingsNavigator}
        options={{
          tabBarIcon: ({ color }) => <TabBarIcon name={Platform.OS === "android" ? "md-settings" : "ios-settings"} color={color} />,
        }}
      />
    </BottomTab.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: { name: string; color: string }) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const MapStack = createStackNavigator<MapParamList>();

function MapNavigator() {
  return (
    <MapStack.Navigator>
      <MapStack.Screen
        name="MapScreen"
        component={MapScreen}
        options={{ headerShown: false }}
      />
      <MapStack.Screen
        name="PartnerScreen"
        component={PartnerScreen}
        options={{ headerShown: false }}
      />
    </MapStack.Navigator>
  );
}

const CartStack = createStackNavigator<CartParamList>();

function CartNavigator() {
  return (
    <CartStack.Navigator>
      <CartStack.Screen
        name="CartScreen"
        component={CartScreen}
        options={{ headerShown: false }}
      />
      <CartStack.Screen
        name="CommandScreen"
        component={CommandScreen}
        options={{ headerShown: false }}
      />
    </CartStack.Navigator>
  );
}

const ReceiptStack = createStackNavigator<ReceiptParamList>();

function ReceiptNavigator() {
  return (
    <ReceiptStack.Navigator>
      <ReceiptStack.Screen
        name="ReceiptScreen"
        component={ReceiptScreen}
        options={{ headerShown: false }}
      />
      <ReceiptStack.Screen
        name="FullReceiptScreen"
        component={FullReceiptScreen}
        options={{ headerShown: false }}
      />
    </ReceiptStack.Navigator>
  );
}

const ProfileStack = createStackNavigator<ProfileParamList>();

function ProfileNavigator() {

  return (
    <ProfileStack.Navigator initialRouteName="ProfileEditScreen">
      <ProfileStack.Screen
        name="ProfileEditScreen"
        component={ProfileScreen}
        options={{ headerShown: false, gestureEnabled: false }}
      />
      <ProfileStack.Screen
        name="ScannerScreen"
        component={ScannerScreen}
        options={{ headerShown: false }}
      />
      <ProfileStack.Screen
        name="InfoScreen"
        component={InfoScreen}
        options={{ headerShown: false }}
      />
    </ProfileStack.Navigator>
  );
}

const SettingsStack = createStackNavigator<SettingsParamList>();

function SettingsNavigator() {

  return (
    <SettingsStack.Navigator initialRouteName="SettingsScreen">
      <SettingsStack.Screen
        name="SettingsScreen"
        component={SettingsScreen}
        options={{ headerShown: false, gestureEnabled: false }}
      />
      <SettingsStack.Screen
        name="CommandMaintainScreen"
        component={CommandMaintainScreen}
        options={{ headerShown: false }}
      />
      <SettingsStack.Screen
        name="ManageCommandScreen"
        component={ManageCommandScreen}
        options={{ headerShown: false }}
      />
      <SettingsStack.Screen
        name="StatsPartnerScreen"
        component={StatsPartnerScreen}
        options={{ headerShown: false }}
      />
      <SettingsStack.Screen
        name="StatsByPartnerScreen"
        component={StatsByPartnerScreen}
        options={{ headerShown: false }}
      />
      <SettingsStack.Screen
        name="StatsByUserScreen"
        component={StatsByUserScreen}
        options={{ headerShown: false }}
      />
      <SettingsStack.Screen
        name="ListPartnerScanScreen"
        component={ListPartnerScanScreen}
        options={{ headerShown: false }}
      />
      <SettingsStack.Screen
        name="ListUserScanScreen"
        component={ListUserScanScreen}
        options={{ headerShown: false }}
      />
      <SettingsStack.Screen
        name="OnboardingScreen"
        component={OnboardingScreen}
        options={{ headerShown: false }}
      />
    </SettingsStack.Navigator>
  );
}

const NotFoundStack = createStackNavigator<NotFoundParamList>();

function NotFoundNavigator() {
  return (
    <NotFoundStack.Navigator>
      <NotFoundStack.Screen
        name="NotFoundScreen"
        component={NotFoundScreen}
        options={{ headerShown: false }}
      />
    </NotFoundStack.Navigator>
  );
}
