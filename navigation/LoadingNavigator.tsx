import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';

import { LoadingParamList } from '../types';
import LoadingScreen from '../screens/LoadingScreen';
import LoginNavigator from './LoginNavigator';
import BottomTabNavigator from './BottomTabNavigator';
import AskScreen from '../screens/AskScreen';

const LoadingTab = createStackNavigator<LoadingParamList>();

export default function LoadingNavigator() {
  return (
    <LoadingTab.Navigator
      initialRouteName="Loading">
      <LoadingTab.Screen
        name="Loading"
        component={LoadingScreen}
        options={{ headerShown: false, animationEnabled: false }}
      />
      <LoadingTab.Screen
        name="Login"
        component={LoginNavigator}
        options={{ headerShown: false, gestureEnabled: false, animationEnabled: false }}
      />
      {/* <LoadingTab.Screen
        name="Ask"
        component={AskScreen}
        options={{ headerShown: false, gestureEnabled: false, animationEnabled: false }}
      /> */}
      <LoadingTab.Screen
        name="Main"
        component={BottomTabNavigator}
        options={{ headerShown: false, gestureEnabled: false, animationEnabled: false }}
      />
    </LoadingTab.Navigator>
  );
}