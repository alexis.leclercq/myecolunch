import { Ionicons } from '@expo/vector-icons';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import { LoginParamList } from '../types';
import FirstScreen from '../screens/Login/FirstScreen';
import RegisterScreen from '../screens/Login/RegisterScreen';
import LoginScreen from '../screens/Login/LoginScreen';
import ResetPasswordScreen from '../screens/Login/ResetPasswordScreen';

const LoginTab = createStackNavigator<LoginParamList>();

export default function LoginNavigator() {
  const colorScheme = useColorScheme();

  return (
    <LoginTab.Navigator
      initialRouteName="First">
      <LoginTab.Screen
        name="First"
        component={FirstScreen}
        options={{ headerShown: false, gestureEnabled: false, animationEnabled: false }}
      />
      <LoginTab.Screen
        name="Register"
        component={RegisterScreen}
        options={{ headerShown: false }}
      />
      <LoginTab.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{ headerShown: false }}
      />
      <LoginTab.Screen
        name="ResetPassword"
        component={ResetPasswordScreen}
        options={{ headerShown: false }}
      />
    </LoginTab.Navigator>
  );
}