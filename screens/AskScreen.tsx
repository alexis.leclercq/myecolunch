import * as React from 'react';
import { StyleSheet, Image } from 'react-native';
import ColoredButton from '../components/ColoredButton';
import SimpleButton from '../components/SimpleButton';
import SplashAnimation from '../components/SplashAnimation';

import { Text, View } from '../components/Themed';
import Layout from '../constants/Layout';

export default function AskScreen(props: any) {

    const goNext = (wanted: string) => props.navigation.navigate('Main', { wanted });

    return (
        <SplashAnimation>
            <View style={{ height: '100%', width: '100%', paddingHorizontal: 20, paddingTop: 50 }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.title}>Je desire</Text>
                    <Image style={styles.littleLogo} source={require('../assets/images/carot.png')} resizeMode={"contain"} />
                </View>
                <View style={styles.alignView}>
                    <SimpleButton style={styles.noButton} text="Petit déjeuner" onPress={() => goNext('breakfast')} />
                    <SimpleButton style={styles.noButton} text="Déjeuner" onPress={() => goNext('lunch')} />
                    <SimpleButton style={styles.noButton} text="Goûter" onPress={() => goNext('tea')} />
                    <SimpleButton style={styles.noButton} text="Diner" onPress={() => goNext('diner')} />
                    <ColoredButton style={styles.noButton} text="Rien pour le moment" onPress={() => goNext('')} />
                </View>
            </View>
        </SplashAnimation>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        fontFamily: 'bold',
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
    noButton: {
        marginVertical: 20,
        width: Layout.window.width * 0.8,
    },
    alignView: {
        marginTop: '30%',
        alignItems: 'center'
    },
    littleLogo: {
        height: 35,
        width: 35
    },
});
