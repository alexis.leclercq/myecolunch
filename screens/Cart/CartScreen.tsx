import React, { useEffect, useState } from 'react';
import { StyleSheet, Image, TouchableOpacity, View as DefaultView, ActivityIndicator, Platform, Alert, ScrollView } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import { Ionicons } from '@expo/vector-icons';

import { Text, View } from '../../components/Themed';
import firebase from '../../constants/firebase';
import Layout from '../../constants/Layout';
import Colors from '../../constants/Colors';
import Input from '../../components/Input';
import SearchIcon from '../../components/SVG/SearchIcon';
import { ICart, IShop } from '../../types';
import CartElement from '../../components/CartElement';

export default function CartScreen(props: any) {
  const scheme = useColorScheme();

  const [elements, setElements] = useState<Array<ICart>>([]);
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  const [selected, setSelected] = useState<Array<IShop>>([]);
  const [lenShop, setLenShop] = useState<number>(0);
  const [searchInput, setSearchInput] = useState<string>("");
  const [itemLen, setItemLen] = useState<number>(0);

  const entityRef = firebase.firestore().collection('cart')

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', async () => {
      await entityRef.orderBy('name').get()
        .then((docRef) => {
          if (docRef.size > 0) {
            let tmpElements: Array<any> = [];
            for (let i in docRef.docs)
              tmpElements.push({ ...docRef.docs[i].data(), id: docRef.docs[i].id });
            setElements(tmpElements);
            setIsLoaded(true);
          }
        })
    })
    return unsubscribe;
  }, [props.navigation])

  const setShop = (name: string, quantity: string, number: number, price: number, type: string) => {
    let shopIndex = -1;
    let tmpSelected = selected;
    let tmpItemLen = itemLen;

    if (type === "plus") {
      if (tmpItemLen + 1 > 15) {
        Alert.alert('Merci de choisir moins de 15 éléments par commande')
        return false;
      } else
        tmpItemLen++;
    } else
      tmpItemLen--;
    for (let i in tmpSelected) {
      if (tmpSelected[i].name === name) {
        if (number == 0)
          tmpSelected.splice(parseInt(i), 1);
        else
          tmpSelected[i].number = number;
        shopIndex = parseInt(i);
      }
    }
    if (shopIndex === -1)
      tmpSelected.push({ name, quantity, number, price });
    setSelected(tmpSelected);
    setLenShop(tmpSelected.length);
    setItemLen(tmpItemLen);
    return true;
  };

  const getAmount = () => {
    let amount = 0;

    for (let i in selected)
      amount += selected[i].number * selected[i].price;
    return amount;
  }

  if (isLoaded)
    return (
      <View style={{ height: '100%', width: '100%', paddingTop: 50, paddingHorizontal: 15 }}>
        <View style={{ flexDirection: 'row', marginBottom: 20, justifyContent: 'space-between' }}>
          <DefaultView style={{ flexDirection: 'row' }}>
            <Text style={styles.title}>Commande</Text>
            <Image style={styles.littleLogo} source={require('../../assets/images/carot.png')} resizeMode={"contain"} />
          </DefaultView>
          <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => lenShop > 0 ? props.navigation.navigate('CommandScreen', { amount: getAmount(), selected }) : Alert.alert('Ajoutez d\'abord des éléments à votre commande ! 😉')}>
            <Ionicons size={35} style={{ marginBottom: -3 }} name={Platform.OS === "android" ? "md-cart" : "ios-cart"} color={Colors.dark.tint} />
            {lenShop > 0 && <DefaultView style={{ backgroundColor: '#FF5454', borderRadius: 50, height: 15, width: 15, position: 'absolute', right: 0, top: 0, justifyContent: 'center', alignItems: 'center' }}><Text style={styles.littleTitle}>{lenShop}</Text></DefaultView>}
          </TouchableOpacity>
        </View>
        <Text style={styles.subTitle}>Fournisseur: Mes Voisins Producteurs</Text>
        <Input style={{ marginBottom: 15 }} value={searchInput} onChangeText={e => setSearchInput(e)} dark={scheme === "dark"} placeholder={"Chercher un fruit ou un légume"} endComponent={() => <SearchIcon color={scheme === "dark" ? Colors.dark.backgroundInverted : Colors.light.backgroundInverted} />} />
        <ScrollView showsVerticalScrollIndicator={false}>
        {elements.map((item, index) => {
            if (item.name.toUpperCase().includes(searchInput.toUpperCase()))
              return (
                <CartElement key={index} name={item.name} price={item.price} quantity={item.quantity} setShop={setShop} canEdit />
              );
            return <View />;
        })}
        </ScrollView>
      </View>
    );
  else
    return <View style={{ height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
      <ActivityIndicator size={"large"} />
    </View>;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    fontFamily: 'bold'
  },
  subTitle: {
    fontSize: 16,
    fontFamily: 'regular',
    marginBottom: 10
  },
  littleTitle: {
    fontSize: 10,
    fontWeight: 'bold',
    fontFamily: 'bold',
    color: Colors.dark.text
  },
  bigTitle: {
    fontSize: 36,
    fontWeight: 'bold',
    fontFamily: 'bold',
    color: Colors.dark.text
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  littleLogo: {
    height: 35,
    width: 35
  },
  block: {
    width: '90%',
    height: Layout.window.height * 0.2,
    backgroundColor: Colors.dark.tint,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.41,
    shadowRadius: 9.11,
    elevation: 14,
  },
  bigBlock: {
    height: Layout.window.height * 0.7,
    alignItems: 'center',
    justifyContent: 'space-evenly'
  }
});
