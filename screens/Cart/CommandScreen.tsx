import React, { useState } from 'react';
import { StyleSheet, Image, TouchableOpacity, View as DefaultView, FlatList, Platform, Alert, Switch, LayoutAnimation } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import { Ionicons } from '@expo/vector-icons';

import { Text, View } from '../../components/Themed';
import firebase from '../../constants/firebase';
import Layout from '../../constants/Layout';
import Colors from '../../constants/Colors';
import CartElement from '../../components/CartElement';
import ColoredButton from '../../components/ColoredButton';
import moment from 'moment';

export default function CommandScreen(props: any) {
    const scheme = useColorScheme();

    const [isLoaded, setIsLoaded] = useState<boolean>(true);
    const [takeBag, setTakebag] = useState<boolean>(false);

    const sendPushNotification = (expoToken: string, name: string) => {
        const userExpoToken = expoToken
        let response = fetch('https://exp.host/--/api/v2/push/send', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                to: userExpoToken,
                sound: 'default',
                body: name + ' a passé une commande',
                title: 'Nouvelle commande'
            })
        })
    }

    const sendCommand = async () => {
        if (isLoaded == false)
            return;
        setIsLoaded(false);
        if (props.route.params.amount < 3) {
            Alert.alert('Tu ne peux commander que pour 3€ minimum !', 'Et oui ! Chez My Eco Lunch nous sommes soucieux de la planète, un minimum est requis pour couvrir le transport de votre commande 🌍 !')
            setIsLoaded(true);
            return;
        }
        let currentUser = firebase.auth().currentUser;
        await firebase.firestore().collection('commands').add({
            selected: props.route.params.selected,
            amount: props.route.params.amount + 0.99 + (takeBag ? 0.60 : 0),
            userId: currentUser?.uid,
            date: moment().toISOString(),
            status: "pending",
            visible: true,
            name: currentUser && currentUser?.displayName ? currentUser?.displayName : currentUser?.email,
            takeBag
        })
        .then(async () => {
            await firebase.firestore().collection('users').where('notifsAdmin', "==", true).where('administrator', "==", true).get().then((docRef) => {
                for (let i in docRef.docs)
                    sendPushNotification(docRef.docs[i].data().expoToken, currentUser && currentUser?.displayName ? currentUser?.displayName : "Une personne");
            })
            setIsLoaded(true);
            Alert.alert('Votre commande a bien été prise en compte', 'Merci de régler cette commande via Pumpkin au numéro de téléphone suivant: 06 52 51 31 29')
            props.navigation.goBack();
        })
        .catch(() => {
            setIsLoaded(true);
            Alert.alert('Une erreur est survenue')
        })
    }

    return (
        <View style={{ height: '100%', width: '100%', paddingTop: 50, paddingHorizontal: 15 }}>
            <View style={{ flexDirection: 'row', marginBottom: 20, justifyContent: 'space-between' }}>
                <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => props.navigation.goBack()}>
                    <Ionicons size={35} style={{ marginBottom: -3 }} name={Platform.OS === "android" ? "md-arrow-back" : "ios-arrow-back"} color={scheme === "dark" ? Colors.dark.text : Colors.light.text} />
                </TouchableOpacity>
                <DefaultView style={{ flexDirection: 'row' }}>
                    <Text style={styles.title}>Panier</Text>
                    <Image style={styles.littleLogo} source={require('../../assets/images/carot.png')} resizeMode={"contain"} />
                </DefaultView>
            </View>
            <FlatList
                showsVerticalScrollIndicator={false}
                data={props.route.params.selected}
                keyExtractor={item => item.name.toString()}
                renderItem={({ item, index }) => (
                    <CartElement key={index} name={item.name} price={item.price} quantity={item.quantity} number={item.number} setShop={false} canEdit={false} />
                )}
                ListFooterComponent={() => (
                    <DefaultView style={{ marginTop: 10 }}>
                        <DefaultView style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                            <Text style={[styles.info, { width: '75%' }]}>Vous pouvez apporter votre propre sac ou choisir ce sac en kraft recyclé et recyclable, c'est aussi super pour la planète ! 🌱 Prix: <Text style={{ fontFamily: 'bold', color: Colors.dark.tint }}>0,60 cts</Text></Text>
                            <Switch
                                trackColor={{ false: Colors.light.tint, true: Colors.light.tint }}
                                thumbColor={takeBag ? "#fff" : Colors.light.tint}
                                ios_backgroundColor="#fff"
                                onValueChange={() => {LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut); setTakebag(!takeBag)}}
                                value={takeBag}
                            />
                        </DefaultView>
                        <Text style={styles.desc}>{Number((props.route.params.amount + 0.99 + (takeBag ? 0.60 : 0)).toFixed(2)) + "€"}</Text>
                        <Text style={styles.info}>Frais de service: <Text style={{ fontFamily: 'bold', color: Colors.dark.tint }}>0,99€</Text>. Ces frais de services correspondent à la logistique de votre commande. De la pousse des produits à votre assiette ! 🍴</Text>
                        <Text style={styles.info}>{"Cette somme devra être payé via l'application Pumpkin."}</Text>
                        <DefaultView style={{ marginVertical: 20, width: '100%', alignItems: 'center' }}>
                            <ColoredButton text={isLoaded ? "Commander" : "Chargement..."} onPress={sendCommand} />
                        </DefaultView>
                    </DefaultView>
                )}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        fontFamily: 'bold'
    },
    littleTitle: {
        fontSize: 10,
        fontWeight: 'bold',
        fontFamily: 'bold'
    },
    desc: {
        fontSize: 24,
        fontWeight: 'bold',
        fontFamily: 'bold',
        color: Colors.dark.tint
    },
    info: {
        fontSize: 15,
        fontFamily: 'light',
        marginTop: 10,
        marginBottom: 5
    },
    bigTitle: {
        fontSize: 36,
        fontWeight: 'bold',
        fontFamily: 'bold',
        color: Colors.dark.text
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
    littleLogo: {
        height: 35,
        width: 35
    },
    block: {
        width: '90%',
        height: Layout.window.height * 0.2,
        backgroundColor: Colors.dark.tint,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowOpacity: 0.41,
        shadowRadius: 9.11,
        elevation: 14,
    },
    bigBlock: {
        height: Layout.window.height * 0.7,
        alignItems: 'center',
        justifyContent: 'space-evenly'
    }
});
