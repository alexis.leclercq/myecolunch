import * as React from 'react';
import { StyleSheet, Image, TouchableOpacity } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import SplashAnimation from '../components/SplashAnimation';

import { Text, View } from '../components/Themed';
import firebase from '../constants/firebase';
import Layout from '../constants/Layout';

export default function HomeScreen(props: any) {
  const scheme = useColorScheme();

  return (
    <SplashAnimation>
      <View style={{ height: '100%', width: '100%', padding: 20, paddingTop: 50 }}>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginBottom: 20, width: '100%' }}>
          <Image source={scheme === "dark" ? require('../assets/images/logow.png') : require('../assets/images/logob.png')} resizeMode="contain" style={{ height: 40, width: Layout.window.width * 0.3 }} />
          <TouchableOpacity activeOpacity={100} onPress={() => props.navigation.navigate('Profile')}>
            <Image source={firebase.auth().currentUser && firebase.auth().currentUser?.photoURL ? { uri: firebase.auth().currentUser?.photoURL } : require('../assets/images/profile.png')} resizeMode="contain" style={{ height: 35, width: 35, borderRadius: 50 }} />
          </TouchableOpacity>
        </View>
        <Text style={styles.title}>Bonjour</Text>
      </View>
    </SplashAnimation>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    fontFamily: 'bold'
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
