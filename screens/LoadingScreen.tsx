import React, { Component } from 'react';
import { ImageBackground } from 'react-native';
import * as SplashScreen from 'expo-splash-screen';

import firebase from '../constants/firebase';

export default class LoadingScreen extends Component<{ navigation: any }> {

    componentDidMount() {
        SplashScreen.preventAutoHideAsync();
        firebase.auth().onAuthStateChanged(async user => {
            if (user) {
                await firebase.firestore().collection('users').doc(user.uid).update({
                    last_login: Date(),
                });
                SplashScreen.hideAsync();
                this.props.navigation.navigate('Main');
            } else {
                SplashScreen.hideAsync();
                this.props.navigation.navigate('Login');
            }
        })
    }

    render() {
        return (
            <ImageBackground source={require('../assets/images/splash.png')} resizeMode="contain" style={{ flex: 1, backgroundColor: '#295828' }} />
        );
    }
}