import * as React from 'react';
import { StyleSheet, Image, ScrollView } from 'react-native';
import { useColorScheme } from 'react-native-appearance';

import { Text, View } from '../../components/Themed';
import Layout from '../../constants/Layout';
import ColoredButton from '../../components/ColoredButton';
import SplashAnimation from '../../components/SplashAnimation';

export default function FirstScreen(props: any) {
  const scheme = useColorScheme();

  return (
    <SplashAnimation>
      <View style={{ height: '100%', width: '100%', paddingTop: 50, paddingHorizontal: 20 }}>
      <ScrollView showsVerticalScrollIndicator={false} >
      <Text style={styles.header}>Qui sommes-nous ?</Text>
      <View style={styles.row}>
        <Image style={styles.networkLogo} source={require('../../assets/images/on1.png')} resizeMode={"contain"} />
        <View style={styles.center}>
          <Text style={styles.subTitle}>Une association</Text>
          <Text style={styles.subTitleLess}>6 étudiants en communication à l'ISCOM Lille, motivés à VOUS aider !</Text>
        </View>
      </View>
      <View style={styles.row}>
        <Image style={styles.networkLogo} source={require('../../assets/images/on2.png')} resizeMode={"contain"} />
        <View style={styles.center}>
          <Text style={styles.subTitle}>Mangez</Text>
          <Text style={styles.subTitleLess}>De savoureux repas grâce à notre carte interactive qui recense les meilleures adresses Lilloises.</Text>
        </View>
      </View>
      <View style={styles.row}>
        <Image style={styles.networkLogo} source={require('../../assets/images/on3.png')} resizeMode={"contain"} />
        <View style={styles.center}>
          <Text style={styles.subTitle}>Économisez</Text>
          <Text style={styles.subTitleLess}>Grâce à l'application My Eco Lunch, bénéficiez de réductions chez nos partenaires.</Text>
        </View>
      </View>
      <View style={styles.row}>
        <Image style={styles.networkLogo} source={require('../../assets/images/on4.png')} resizeMode={"contain"} />
        <View style={styles.center}>
          <Text style={styles.subTitle}>Cuisinez</Text>
          <Text style={styles.subTitleLess}>De délicieuses recettes triées sur le volet par notre équipe. Suivez les tutoriels vidéo de notre équipe via l'application !</Text>
        </View>
      </View>
      <View style={styles.row}>
        <Image style={styles.networkLogo} source={require('../../assets/images/on5.png')} resizeMode={"contain"} />
        <View style={styles.center}>
          <Text style={styles.subTitle}>Simplifiez-vous la vie !</Text>
          <Text style={styles.subTitleLess}>Notre service de paniers fraîcheurs vous simplifiera la vie. En un clic, vous pourrez commander et récupérer vos produits frais au campus.</Text>
        </View>
      </View>
      <ColoredButton style={styles.joinButton} text="C'est parti !" onPress={() => props.navigation.navigate('LoginScreen', { scheme: scheme })} />
      </ScrollView>
      </View>
    </SplashAnimation>
  );
}

const styles = StyleSheet.create({
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  signButton: {
    marginTop: 40,
    marginBottom: 20,
    width: Layout.window.width * 0.6,
  },
  joinButton: {
    width: Layout.window.width * 0.6,
    alignSelf: 'center',
    marginTop: 40
  },
  blankSpace: {
    height: Layout.window.height * 0.3
  },
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      fontSize: 24,
      fontWeight: 'bold',
      fontFamily: 'bold',
      paddingBottom: 20
    },
    header: {
      fontSize: 24,
      fontWeight: 'bold',
      fontFamily: 'bold',
      paddingBottom: 40,
      paddingTop: 10,
      textAlign: 'center'
    },
    row: {
      flexDirection: 'row',
    },
    center: {
      justifyContent: 'center',
      flex: 1
    },
    subTitle: {
      fontSize: 17,
      fontFamily: 'bold',
    },
    subTitleLess: {
      fontSize: 13,
      fontFamily: 'light',
      paddingTop: 5,
    },
    networkLogo: {
      height: Layout.window.width * 0.2,
      width: Layout.window.width * 0.2,
      margin: 10
    }
  });