import * as React from 'react';
import { StyleSheet, Image, KeyboardAvoidingView, Platform, ActivityIndicator, TouchableOpacity } from 'react-native';

import { Text, View } from '../../components/Themed';
import ColoredButton from '../../components/ColoredButton';
import Input from '../../components/Input';
import firebase from '../../constants/firebase';
import Layout from '../../constants/Layout';

interface LoginProps {
    route: any,
    navigation: any,
}

export default class LoginScreen extends React.Component<LoginProps, any> {

    constructor(props: LoginProps) {
        super(props);
    }

    state = {
        email: '',
        password: '',
        loading: false,
        errorMessage: '',
    };

    emailInput: any;
    passwordInput: any;

    submitEmail() {
        if (this.passwordInput)
            this.passwordInput.focus();
    }

    onChangeEmail(email: string) {
        this.setState({ email });
    }

    submitPassword() {
        this.validate();
    }

    onChangePassword(password: string) {
        this.setState({ password });
    }

    async validate() {
        if (this.state.email.length == 0 || this.state.password.length == 0) {
            this.setState({ errorMessage: 'Merci de remplir tous les champs' });
            return;
        }
        this.setState({ loading: true });
        await firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
            .then(async () => {
                const credential = firebase.auth.EmailAuthProvider.credential(this.state.email, this.state.password);
                firebase.auth().signInWithCredential(credential)
                    .then(async () => {
                        this.setState({ loading: false });
                        this.props.navigation.navigate('Main');
                    });
            })
            .catch(error => {
                this.setState({ loading: false, errorMessage: "E-mail ou mot de passe incorrect" });
            })
    }

    render() {
        return (
            <View style={{ alignItems: 'center', height: '100%', width: '100%', justifyContent: 'center', padding: 20 }}>
                <Image source={require('../../assets/images/icon.png')} resizeMode="contain" style={{ width: Layout.window.width * 0.5, marginBottom: 20 }} />
                <Text style={styles.title} adjustsFontSizeToFit>Se connecter</Text>
                {Platform.OS === 'ios' && <KeyboardAvoidingView style={styles.registerContainer} behavior="padding">
                    <Text style={styles.subtitle} adjustsFontSizeToFit>E-mail</Text>
                    <Input returnKeyType="next" placeholder="E-mail" onRef={(ref) => this.emailInput = ref} onSubmitEditing={() => this.submitEmail()} maxLength={40} onChangeText={e => this.onChangeEmail(e)} style={styles.input} dark={this.props.route.params.scheme === "dark"} autoCapitalize="none" autoCompleteType="email" autoCorrect={false} value={this.state.email} />
                    <Text style={styles.subtitle} adjustsFontSizeToFit>Mot de passe</Text>
                    <Input returnKeyType="done" placeholder="Mot de passe" onRef={(ref) => this.passwordInput = ref} onSubmitEditing={() => this.submitPassword()} onChangeText={e => this.onChangePassword(e)} style={styles.input} dark={this.props.route.params.scheme === "dark"} secureTextEntry={true} autoCapitalize="none" autoCompleteType="password" autoCorrect={false} value={this.state.password} />
                    {this.state.errorMessage.length > 0 && <Text style={[styles.subtitle, { color: '#ff5454' }]} adjustsFontSizeToFit>{this.state.errorMessage}</Text>}
                </KeyboardAvoidingView>}
                {Platform.OS === 'android' && <View style={styles.registerContainer}>
                    <Text style={styles.subtitle} adjustsFontSizeToFit>E-mail</Text>
                    <Input returnKeyType="next" placeholder="E-mail" onRef={(ref) => this.emailInput = ref} onSubmitEditing={() => this.submitEmail()} maxLength={40} onChangeText={e => this.onChangeEmail(e)} style={styles.input} dark={this.props.route.params.scheme === "dark"} autoCapitalize="none" autoCompleteType="email" autoCorrect={false} value={this.state.email} />
                    <Text style={styles.subtitle} adjustsFontSizeToFit>Mot de passe</Text>
                    <Input returnKeyType="done" placeholder="Mot de passe" onRef={(ref) => this.passwordInput = ref} onSubmitEditing={() => this.submitPassword()} onChangeText={e => this.onChangePassword(e)} style={styles.input} dark={this.props.route.params.scheme === "dark"} secureTextEntry={true} autoCapitalize="none" autoCompleteType="password" autoCorrect={false} value={this.state.password} />
                    {this.state.errorMessage.length > 0 && <Text style={[styles.subtitle, { color: '#ff5454' }]} adjustsFontSizeToFit>{this.state.errorMessage}</Text>}
                </View>}
                <TouchableOpacity style={{ width: '100%', marginVertical: 20 }} onPress={() => this.props.navigation.navigate('Register', { scheme: this.props.route.params.scheme })}>
                    <Text style={[styles.desc, { textAlign: 'left' }]} adjustsFontSizeToFit>Créer un compte</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('ResetPassword', { scheme: this.props.route.params.scheme })}>
                    <Text style={styles.desc} adjustsFontSizeToFit>J'ai oublié mon mot de passe</Text>
                </TouchableOpacity>
                <ColoredButton style={styles.signButton} text="Connexion" onPress={() => this.validate()} />
                {this.state.loading && <View style={{ position: 'absolute', top: 0, left: 0, height: Layout.window.height, width: Layout.window.width, backgroundColor: 'rgba(255, 255, 255, 0.3)', alignItems: 'center', justifyContent: 'center' }}><ActivityIndicator size="large" /></View>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    registerContainer: {
        marginVertical: 20,
        width: '100%',
    },
    title: {
        fontSize: 22,
        fontFamily: 'bold',
        textAlign: 'center',
    },
    subtitle: {
        fontSize: 18,
        fontFamily: 'light',
        textAlign: 'left',
    },
    desc: {
        fontSize: 12,
        fontFamily: 'light',
        textAlign: 'left',
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
    signButton: {
        marginVertical: 20,
        width: Layout.window.width * 0.6,
    },
    input: {
        marginVertical: 20,
        width: '100%',
    }
});
