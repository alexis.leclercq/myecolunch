import * as React from 'react';
import { StyleSheet, Image, KeyboardAvoidingView, Platform, ActivityIndicator, TouchableOpacity } from 'react-native';

import { Text, View } from '../../components/Themed';
import ColoredButton from '../../components/ColoredButton';
import Input from '../../components/Input';
import firebase from '../../constants/firebase';
import Layout from '../../constants/Layout';

function validateEmail(email: string) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

interface RegisterProps {
    route: any,
    navigation: any,
}

export default class RegisterScreen extends React.Component<RegisterProps, any> {

    constructor(props: RegisterProps) {
        super(props);
    }

    state = {
        email: '',
        password: '',
        emailStatus: 'none',
        passwordStatus: 'none',
        loading: false,
        errorMessage: '',
    };

    emailInput: any;
    passwordInput: any;

    submitEmail() {
        if (validateEmail(this.state.email) && this.state.email.length > 5)
            this.setState({ emailStatus: 'ok' });
        else
            this.setState({ emailStatus: 'ko' });
        if (this.passwordInput)
            this.passwordInput.focus();
    }

    onChangeEmail(email: string) {
        if (validateEmail(email) && email.length > 5)
            this.setState({ emailStatus: 'ok' });
        else
            this.setState({ emailStatus: 'ko' });
        this.setState({ email });
    }

    submitPassword() {
        if (this.state.password.length > 5)
            this.setState({ passwordStatus: 'ok' });
        else
            this.setState({ passwordStatus: 'ko' });
    }

    onChangePassword(password: string) {
        if (password.length > 5)
            this.setState({ passwordStatus: 'ok' });
        else
            this.setState({ passwordStatus: 'ko' });
        this.setState({ password });
    }

    async validate() {
        if (this.state.emailStatus !== 'ok' || this.state.passwordStatus !== 'ok') {
            this.setState({ errorMessage: 'Merci de remplir tous les champs' });
            return;
        }
        if (!this.state.email.endsWith("@my-digital-school.org") && !this.state.email.endsWith("@iscom.org") && !this.state.email.endsWith("@iscom.fr") && !this.state.email.endsWith("@mbway.org") && !this.state.email.endsWith("@ipacfactory.org") && !this.state.email.endsWith("@win-sport-school.org") && !this.state.email.endsWith("@mydigitalschool.org") && !this.state.email.endsWith("@tunon.org")) {
            this.setState({ errorMessage: 'Vous devez avoir une adresse-mail du campus pour pouvoir créer un compte' });
            return;
        }
        this.setState({ loading: true });
        await firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then(async () => {
                const credential = firebase.auth.EmailAuthProvider.credential(this.state.email, this.state.password);
                firebase.auth().signInWithCredential(credential)
                    .then(async () => {
                        if (firebase.auth().currentUser) {
                            await firebase.firestore().collection('users').doc(firebase.auth().currentUser?.uid).set({
                                last_login: Date(),
                                email: this.state.email,
                                points: 0
                            });
                        }
                        this.setState({ loading: false });
                        await firebase.auth().currentUser?.sendEmailVerification();
                        this.props.navigation.navigate('Main');
                    });
            })
            .catch(error => {
                this.setState({ loading: false, errorMessage: error.code === 'auth/email-already-in-use' ? "Cette adresse e-mail est déjà utilisée" : error.message });
            })
    }

    render() {
        return (
            <View style={{ alignItems: 'center', height: '100%', width: '100%', justifyContent: 'center', padding: 20 }}>
                <Image source={require('../../assets/images/icon.png')} resizeMode="contain" style={{ width: Layout.window.width * 0.5, marginBottom: 20 }} />
                <Text style={styles.title} adjustsFontSizeToFit>S'inscrire</Text>
                {Platform.OS === 'ios' && <KeyboardAvoidingView style={styles.registerContainer} behavior="padding">
                    <Text style={styles.subtitle} adjustsFontSizeToFit>E-mail</Text>
                    <Input returnKeyType="next" placeholder="E-mail" endComponent={() => this.state.emailStatus !== 'none' && <Image source={this.state.emailStatus === 'ok' ? require('../../assets/images/checked.png') : require('../../assets/images/wrong.png')} style={{ height: 20, width: 30, marginTop: 5 }} resizeMode="contain" />} onRef={(ref) => this.emailInput = ref} onSubmitEditing={() => this.submitEmail()} maxLength={40} onChangeText={e => this.onChangeEmail(e)} style={styles.input} dark={this.props.route.params.scheme === "dark"} autoCapitalize="none" autoCompleteType="email" autoCorrect={false} value={this.state.email} />
                    <Text style={styles.subtitle} adjustsFontSizeToFit>Mot de passe</Text>
                    <Input returnKeyType="done" placeholder="Mot de passe" endComponent={() => this.state.passwordStatus !== 'none' && <Image source={this.state.passwordStatus === 'ok' ? require('../../assets/images/checked.png') : require('../../assets/images/wrong.png')} style={{ height: 20, width: 30, marginTop: 5 }} resizeMode="contain" />} onRef={(ref) => this.passwordInput = ref} onSubmitEditing={() => this.submitPassword()} onChangeText={e => this.onChangePassword(e)} style={styles.input} dark={this.props.route.params.scheme === "dark"} secureTextEntry={true} autoCapitalize="none" autoCompleteType="password" autoCorrect={false} value={this.state.password} />
                    {this.state.errorMessage.length > 0 && <Text style={[styles.subtitle, { color: '#ff5454' }]} adjustsFontSizeToFit>{this.state.errorMessage}</Text>}
                </KeyboardAvoidingView>}
                {Platform.OS === 'android' && <View style={styles.registerContainer}>
                    <Text style={styles.subtitle} adjustsFontSizeToFit>E-mail</Text>
                    <Input returnKeyType="next" placeholder="E-mail" endComponent={() => this.state.emailStatus !== 'none' && <Image source={this.state.emailStatus === 'ok' ? require('../../assets/images/checked.png') : require('../../assets/images/wrong.png')} style={{ height: 20, width: 30, marginTop: 5 }} resizeMode="contain" />} onRef={(ref) => this.emailInput = ref} onSubmitEditing={() => this.submitEmail()} maxLength={40} onChangeText={e => this.onChangeEmail(e)} style={styles.input} dark={this.props.route.params.scheme === "dark"} autoCapitalize="none" autoCompleteType="email" autoCorrect={false} value={this.state.email} />
                    <Text style={styles.subtitle} adjustsFontSizeToFit>Mot de passe</Text>
                    <Input returnKeyType="done" placeholder="Mot de passe" endComponent={() => this.state.passwordStatus !== 'none' && <Image source={this.state.passwordStatus === 'ok' ? require('../../assets/images/checked.png') : require('../../assets/images/wrong.png')} style={{ height: 20, width: 30, marginTop: 5 }} resizeMode="contain" />} onRef={(ref) => this.passwordInput = ref} onSubmitEditing={() => this.submitPassword()} onChangeText={e => this.onChangePassword(e)} style={styles.input} dark={this.props.route.params.scheme === "dark"} secureTextEntry={true} autoCapitalize="none" autoCompleteType="password" autoCorrect={false} value={this.state.password} />
                    {this.state.errorMessage.length > 0 && <Text style={[styles.subtitle, { color: '#ff5454' }]} adjustsFontSizeToFit>{this.state.errorMessage}</Text>}
                </View>}
                <TouchableOpacity style={{ width: '100%', marginVertical: 20 }} onPress={() => this.props.navigation.navigate('LoginScreen', { scheme: this.props.route.params.scheme })}>
                    <Text style={[styles.desc, { textAlign: 'left' }]} adjustsFontSizeToFit>J'ai déjà un compte</Text>
                </TouchableOpacity>
                <Text style={styles.desc} adjustsFontSizeToFit>Saisissez l'e-mail que vous utilisez sur le campus pour que votre compte soit validé et pour accéder aux promotions des partenaires</Text>
                <Text style={styles.desc} adjustsFontSizeToFit>En cliquant sur "Suivant" vous acceptez nos termes et conditions d'utilisations</Text>
                <ColoredButton style={styles.signButton} text="Suivant" onPress={() => this.validate()} />
                {this.state.loading && <View style={{ position: 'absolute', top: 0, left: 0, height: Layout.window.height, width: Layout.window.width, backgroundColor: 'rgba(255, 255, 255, 0.3)', alignItems: 'center', justifyContent: 'center' }}><ActivityIndicator size="large" /></View>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    registerContainer: {
        marginVertical: 20,
        width: '100%',
    },
    title: {
        fontSize: 22,
        fontFamily: 'bold',
        textAlign: 'center',
    },
    subtitle: {
        fontSize: 18,
        fontFamily: 'light',
        textAlign: 'left',
    },
    desc: {
        fontSize: 12,
        fontFamily: 'light',
        textAlign: 'center',
        marginTop: 10
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
    signButton: {
        marginVertical: 20,
        width: Layout.window.width * 0.6,
    },
    input: {
        marginVertical: 20,
        width: '100%',
    }
});
