import * as React from 'react';
import { StyleSheet, Image, KeyboardAvoidingView, Platform, ActivityIndicator } from 'react-native';

import { Text, View } from '../../components/Themed';
import ColoredButton from '../../components/ColoredButton';
import Input from '../../components/Input';
import firebase from '../../constants/firebase';
import Layout from '../../constants/Layout';

interface ResetPasswordProps {
    route: any,
    navigation: any,
}

export default class ResetPasswordScreen extends React.Component<ResetPasswordProps, any> {

    constructor(props: ResetPasswordProps) {
        super(props);
    }

    state = {
        email: '',
        loading: false,
        errorMessage: '',
        successMessage: '',
    };

    emailInput: any;

    submitEmail() {
        this.validate();
    }

    onChangeEmail(email: string) {
        this.setState({ email });
    }

    async validate() {
        if (this.state.email.length == 0) {
            this.setState({ errorMessage: 'Merci de remplir tous les champs' });
            return;
        }
        this.setState({ loading: true });
        await firebase.auth().sendPasswordResetEmail(this.state.email)
        .then(() => {
            this.setState({ loading: false, successMessage: 'Un e-mail de récupération a été envoyé' });
        })
        .catch(() => {
            this.setState({ loading: false, successMessage: 'E-mail incorrecte' });
        });
    }

    render() {
        return (
            <View style={{ alignItems: 'center', height: '100%', width: '100%', justifyContent: 'center', padding: 20 }}>
                <Image source={require('../../assets/images/icon.png')} resizeMode="contain" style={{ width: Layout.window.width * 0.5, marginBottom: 20 }} />
                <Text style={styles.title} adjustsFontSizeToFit>Réinitialiser son mot de passe</Text>
                {Platform.OS === 'ios' && <KeyboardAvoidingView style={styles.registerContainer} behavior="padding">
                    <Text style={styles.subtitle} adjustsFontSizeToFit>E-mail</Text>
                    <Input returnKeyType="done" placeholder="E-mail" onRef={(ref) => this.emailInput = ref} onSubmitEditing={() => this.submitEmail()} maxLength={40} onChangeText={e => this.onChangeEmail(e)} style={styles.input} dark={this.props.route.params.scheme === "dark"} autoCapitalize="none" autoCompleteType="email" autoCorrect={false} value={this.state.email} />
                    {this.state.errorMessage.length > 0 && <Text style={[styles.subtitle, { color: '#ff5454' }]} adjustsFontSizeToFit>{this.state.errorMessage}</Text>}
                    {this.state.successMessage.length > 0 && <Text style={[styles.subtitle, { color: '#5cd404' }]} adjustsFontSizeToFit>{this.state.successMessage}</Text>}
                </KeyboardAvoidingView>}
                {Platform.OS === 'android' && <View style={styles.registerContainer}>
                    <Text style={styles.subtitle} adjustsFontSizeToFit>E-mail</Text>
                    <Input returnKeyType="done" placeholder="E-mail" onRef={(ref) => this.emailInput = ref} onSubmitEditing={() => this.submitEmail()} maxLength={40} onChangeText={e => this.onChangeEmail(e)} style={styles.input} dark={this.props.route.params.scheme === "dark"} autoCapitalize="none" autoCompleteType="email" autoCorrect={false} value={this.state.email} />
                    {this.state.errorMessage.length > 0 && <Text style={[styles.subtitle, { color: '#ff5454' }]} adjustsFontSizeToFit>{this.state.errorMessage}</Text>}
                    {this.state.successMessage.length > 0 && <Text style={[styles.subtitle, { color: '#5cd404' }]} adjustsFontSizeToFit>{this.state.successMessage}</Text>}
                </View>}
                <ColoredButton style={styles.signButton} text="Réinitialiser" onPress={() => this.validate()} />
                {this.state.loading && <View style={{ position: 'absolute', top: 0, left: 0, height: Layout.window.height, width: Layout.window.width, backgroundColor: 'rgba(255, 255, 255, 0.3)', alignItems: 'center', justifyContent: 'center' }}><ActivityIndicator size="large" /></View>}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    registerContainer: {
        marginVertical: 30,
        width: '100%',
    },
    title: {
        fontSize: 18,
        fontFamily: 'bold',
        textAlign: 'center',
    },
    subtitle: {
        fontSize: 18,
        fontFamily: 'light',
        textAlign: 'left',
    },
    desc: {
        fontSize: 12,
        fontFamily: 'light',
        textAlign: 'left',
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
    signButton: {
        marginVertical: 20,
        width: Layout.window.width * 0.6,
    },
    input: {
        marginVertical: 20,
        width: '100%',
    }
});
