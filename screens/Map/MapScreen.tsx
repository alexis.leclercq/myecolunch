import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, Button, Image, TouchableOpacity, View as DefaultView, ScrollView, LayoutAnimation } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import SplashAnimation from '../../components/SplashAnimation';

import { Text, View, ViewInverted, TouchableOpacity as ThemedTouchable, TextInverted } from '../../components/Themed';
import firebase from '../../constants/firebase';
import Layout from '../../constants/Layout';

import MapView, { Marker, Region } from 'react-native-maps';
import Input from '../../components/Input';
import SearchIcon from '../../components/SVG/SearchIcon';
import Colors from '../../constants/Colors';
import { PartnersParamList } from '../../types';
import { geohashQueryBounds, distanceBetween } from 'geofire-common';
import * as Permissions from 'expo-permissions';
import { Notifications } from 'expo';

export default function MapScreen(props: any) {
  const scheme = useColorScheme();
  const [markers, setMarkers] = useState<Array<PartnersParamList> | null>([]);
  const [ready, setReady] = useState<boolean>(false);
  const [lastRegion, setLastRegion] = useState<Region>();
  const [alreadyScanned, setAlreadyScanned] = useState<Array<string>>([]);
  const [searchInput, setSearchInput] = useState<string>();
  const [searched, setSearched] = useState<Array<PartnersParamList> | null>(null);
  const [selected, setSelected] = useState<number | null>(null);

  const entityRef = firebase.firestore().collection('partners')

  var refMapView: MapView | null;
  const scrollViewRef = useRef();

  const goToMyRegion = () => {
    let refMap = refMapView;
    navigator.geolocation.getCurrentPosition((pos) => {
      if (refMap)
        refMap.animateToRegion({ latitude: pos.coords.latitude, longitude: pos.coords.longitude, latitudeDelta: 0.15, longitudeDelta: 0.15 });
    });
  };

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', async () => {
      navigator.geolocation.getCurrentPosition(async (pos) => {
        const center = [pos.coords.latitude, pos.coords.longitude];
        const area = geohashQueryBounds(center, 10 * 1000);
        let tmpMarkers: Array<any> = [];
        let tmpScanned: Array<string> = [];
        for (const a of area) {
          if (!tmpScanned.includes(a[0]) && !tmpScanned.includes(a[1])) {
            tmpScanned.push(a[0]);
            tmpScanned.push(a[1]);
            await entityRef.orderBy('geohash').startAt(a[0]).endAt(a[1]).get()
              .then((docRef) => {
                if (docRef.size > 0) {
                  for (let i in docRef.docs) {
                    const distanceInKm = distanceBetween([docRef.docs[i].data().coordinate.latitude, docRef.docs[i].data().coordinate.longitude], center);
                    tmpMarkers.push({ ...docRef.docs[i].data(), id: docRef.docs[i].id, distance: distanceInKm.toPrecision(4) });
                  }
                }
              })
          }
        }
        setMarkers(tmpMarkers);
        setAlreadyScanned(tmpScanned);
      })
      registerForPushNotifications();
    });
    return unsubscribe;
  }, [props.navigation])

  const registerForPushNotifications = async () => {
    //Get the current users id So you can post the token to the user in your database
    const currentUser = firebase.auth().currentUser?.uid
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;
    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
      return;
    }
    // Get the token that uniquely identifies this device
    try {
      let token = await Notifications.getExpoPushTokenAsync();
      // POST the token to your backend server from where you can retrieve it to send push notifications.
      await firebase.firestore().collection('users').doc(currentUser).update({ expoToken: token });
    }
    catch (error) {
      console.log(error)
    }
  }

  const updateMarkers = async (region: Region) => {
    if (ready && region.latitudeDelta <= 0.3 && region.longitudeDelta <= 0.3) {
      const center = [region.latitude, region.longitude];
      const area = geohashQueryBounds(center, 5 * 1000);
      let tmpMarkers: Array<any> = markers ? markers : [];
      let tmpScanned: Array<string> = alreadyScanned ? alreadyScanned : [];
      for (const a of area) {
        if (!tmpScanned.includes(a[0]) && !tmpScanned.includes(a[1])) {
          tmpScanned.push(a[0]);
          tmpScanned.push(a[1]);
          await entityRef.orderBy('geohash').startAt(a[0]).endAt(a[1]).get()
            .then((docRef) => {
              if (docRef.size > 0) {
                for (let i in docRef.docs) {
                  let isPresent: boolean = false;
                  for (let j in tmpMarkers)
                    if (tmpMarkers[j].id === docRef.docs[i].id)
                      isPresent = true;
                  if (isPresent == false) {
                    const distanceInKm = distanceBetween([docRef.docs[i].data().coordinate.latitude, docRef.docs[i].data().coordinate.longitude], center);
                    tmpMarkers.push({ ...docRef.docs[i].data(), id: docRef.docs[i].id, distance: distanceInKm.toPrecision(4) });
                  }
                }
              }
            })
        }
      }
      setMarkers(tmpMarkers);
      setAlreadyScanned(tmpScanned);
    }
  };

  const searchPartners = async () => {
    if (searchInput && searchInput.length > 0) {
      entityRef.where('nameArray', 'array-contains', searchInput.toLocaleLowerCase().replaceAll(' ', '')).limit(10).get()
        .then((docRef) => {
          let newSearched: Array<any> = [];
          if (!docRef.empty)
            for (let i in docRef.docs)
              newSearched.push({ ...docRef.docs[i].data(), id: docRef.docs[i].id });
          setSearched(newSearched);
          if (newSearched.length > 0) {
            for (let i in markers) {
              if (newSearched[0].name === markers[parseInt(i)].name) {
                refMapView?.animateToRegion({ latitude: newSearched[0].coordinate.latitude, longitude: newSearched[0].coordinate.longitude, latitudeDelta: 0.03, longitudeDelta: 0.03 });
                setSelected(parseInt(i));
              }
            }
          }
        });
    }
  };

  const onMapReady = () => {
    if (!ready)
      setReady(true);
  };

  const pressPartner = (item: PartnersParamList, index: number) => {
    if (selected === index) {
      props.navigation.navigate('PartnerScreen', { props: item })
    } else {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      setSelected(index);
      refMapView?.animateToRegion({ latitude: item.coordinate.latitude, longitude: item.coordinate.longitude, latitudeDelta: 0.03, longitudeDelta: 0.03 });
    }
  }

  const getDistanceText = (item: any) => {
    let fl = parseFloat(parseFloat(item.distance).toFixed(2));
    if (parseFloat(item?.distance) < 1)
      return ("À " + fl * 1000 + " m");
    else
      return ("À " + item.distance + " km");
  }

  return (
    <SplashAnimation>
      <View style={styles.container}>
        <MapView ref={(map) => { refMapView = map; }} onRegionChange={r => setLastRegion(r)} onLayout={goToMyRegion} onMapReady={onMapReady} showsCompass={false} showsUserLocation style={styles.map} onRegionChangeComplete={region => updateMarkers(region)}>
          {markers && markers.map((marker, index) => {
            if (lastRegion && lastRegion?.latitudeDelta <= 0.3)
              return (
                <Marker
                  key={index}
                  coordinate={marker.coordinate}
                  // title={marker.name}
                  style={{ justifyContent: 'center', alignItems: 'center' }}
                  onPress={() => { setSelected(index); scrollViewRef.current.scrollTo({x: ((Layout.window.width * 0.4) * index) + (20 * index), animated: true}); LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut); refMapView?.animateToRegion({ latitude: marker.coordinate.latitude, longitude: marker.coordinate.longitude, latitudeDelta: 0.03, longitudeDelta: 0.03 }); }}
                >
                  <Image source={require('../../assets/images/marker.png')} style={{ height: 90 }} resizeMode={"contain"} />
                </Marker>
              );
            return (<View key={index} />);
          })}
        </MapView>
        <DefaultView style={{ position: 'absolute', top: 50, alignItems: 'center' }}>
          <Input value={searchInput} search onSubmitEditing={searchPartners} onChangeText={e => setSearchInput(e)} dark={scheme === "dark"} placeholder={"Chercher un partenaire"} endComponent={() => <SearchIcon color={scheme === "dark" ? Colors.dark.backgroundInverted : '#fff'} />} />
          {searched && (
            <>
              <ViewInverted style={styles.searchBlock}>
                {searched.map((item, index) => (
                  <TouchableOpacity key={index} style={styles.searchView} onPress={() => refMapView?.animateToRegion({ latitude: item.coordinate.latitude, longitude: item.coordinate.longitude, latitudeDelta: 0.005, longitudeDelta: 0.005 })}>
                    <DefaultView style={styles.row}>
                      <Image source={item.url ? { uri: item.url } : require('../../assets/images/icon.png')} style={{ height: 50, width: 50, borderRadius: 50 }} resizeMode={"contain"} />
                      <DefaultView style={styles.searchInfoView}>
                        <Text style={styles.searchPartnerTitle}>{item.name}</Text>
                        <Text style={styles.searchPartnerDesc}>{item.address}</Text>
                      </DefaultView>
                    </DefaultView>
                  </TouchableOpacity>
                ))}
              </ViewInverted>
              <ViewInverted style={styles.searchClear}>
                <TouchableOpacity onPress={() => { setSearched(null); setSearchInput("") }}>
                  <Text style={styles.searchClearText}>Effacer</Text>
                </TouchableOpacity>
              </ViewInverted>
            </>
          )}
        </DefaultView>
        <DefaultView style={{ position: 'absolute', bottom: 10, alignItems: 'center' }}>
          {markers && (
            <ScrollView ref={scrollViewRef} style={styles.bottomBlock} horizontal showsHorizontalScrollIndicator={false}>
              {markers.map((item, index) => {
                if (lastRegion && lastRegion?.latitudeDelta <= 0.3)
                  return (
                    <ThemedTouchable key={index} style={[styles.bottomView, { height: selected === index ? '100%' : '80%', marginTop: selected === index ? 0 : Layout.window.height * 0.05, width: selected === index ? Layout.window.width * 0.5 : Layout.window.width * 0.4, borderRadius: 25, overflow: 'hidden' }]} onPress={() => pressPartner(item, index)}>
                      <Image source={item.urlView ? { uri: item.urlView } : require('../../assets/images/icon.png')} style={{ height: Layout.window.width * 0.35, width: Layout.window.width * 0.5, borderTopLeftRadius: 25, borderTopRightRadius: 25 }} resizeMode={"cover"} />
                      <DefaultView style={styles.bottomLittleView}>
                        <TextInverted style={styles.searchPartnerTitle}>{item.name}</TextInverted>
                        <TextInverted style={styles.bottomPartnerDesc}>{getDistanceText(item)}</TextInverted>
                        {selected === index && <TextInverted style={styles.bottomPartnerDesc}>Plus d'informations →</TextInverted>}
                      </DefaultView>
                    </ThemedTouchable>
                  );
                return <View key={index} />;
              })}
            </ScrollView>
          )}
        </DefaultView>
      </View>
    </SplashAnimation>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    width: Layout.window.width,
    height: Layout.window.height,
  },
  marker: {
    height: 40,
    marginBottom: 20
  },
  circle: {
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
    backgroundColor: Colors.dark.tint,
  },
  pinText: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    marginBottom: 10,
    fontFamily: 'light'
  },
  searchBlock: {
    borderRadius: 15,
    width: '95%',
    marginTop: 10
  },
  searchView: {
    margin: 5,
    padding: 5
  },
  searchClear: {
    marginTop: 10,
    borderRadius: 25,
    alignSelf: 'flex-end',
    marginRight: 10,
    padding: 5,
    paddingHorizontal: 10
  },
  searchClearText: {
    textAlign: 'center',
    fontFamily: 'light'
  },
  searchPartnerTitle: {
    fontSize: 16,
    fontFamily: 'regular'
  },
  searchPartnerDesc: {
    fontSize: 14,
    fontFamily: 'light'
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  searchInfoView: {
    marginHorizontal: 10
  },
  bottomBlock: {
    height: Layout.window.height * 0.25,
    flex: 1
  },
  bottomView: {
    borderRadius: 25,
    marginHorizontal: 10,
  },
  bottomLittleView: {
    padding: 10,
    width: '100%',
    height: '40%',
    position: 'absolute',
    bottom: 0,
    left: 0,
    backgroundColor: Colors.dark.tint
  },
  bottomPartnerDesc: {
    fontSize: 13,
    fontFamily: 'light'
  },
});