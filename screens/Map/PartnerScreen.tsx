import { Ionicons } from '@expo/vector-icons';
import React from 'react';
import { StyleSheet, Image, TouchableOpacity, Platform, View as DefaultView, ScrollView } from 'react-native';
import { useColorScheme } from 'react-native-appearance';

import { Text, View } from '../../components/Themed';
import Colors from '../../constants/Colors';
import Layout from '../../constants/Layout';

export default function PartnerScreen(props: any) {
    const scheme = useColorScheme();

    return (
        <View style={{ height: '100%', width: '100%', paddingTop: 50 }}>
            <View style={{ flexDirection: 'row', marginBottom: 20, justifyContent: 'space-between', marginHorizontal: 20 }}>
                <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => props.navigation.goBack()}>
                    <Ionicons size={35} style={{ marginBottom: -3 }} name={Platform.OS === "android" ? "md-arrow-back" : "ios-arrow-back"} color={scheme === "dark" ? Colors.dark.text : Colors.light.text} />
                </TouchableOpacity>
                <DefaultView style={{ flexDirection: 'row' }}>
                    <Text style={styles.title}>Partenaire</Text>
                    <Image style={styles.littleLogo} source={require('../../assets/images/carot.png')} resizeMode={"contain"} />
                </DefaultView>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Image style={styles.bigbigLogo} source={{ uri: props.route.params.props.urlView }} resizeMode={"cover"} />
                <DefaultView style={{ marginHorizontal: 20, marginTop: -75 }}>
                    <DefaultView style={{ flexDirection: 'row', alignItems: 'flex-end', marginBottom: 20 }}>
                        <Image style={styles.bigLogo} source={{ uri: props.route.params.props.url }} resizeMode={"contain"} />
                        <Text style={styles.title}>{props.route.params.props.name}</Text>
                    </DefaultView>
                    <Text style={styles.subTitle}>{props.route.params.props.description}</Text>
                    <Text style={styles.subTitleBold}>{props.route.params.props.address}</Text>
                    <Text style={styles.subTitleBold}>Horaires: <Text style={styles.subTitle}>{props.route.params.props.hours}</Text></Text>
                    <Text style={styles.subTitleBold}>Promo: <Text style={styles.subTitleMain}>{props.route.params.props.promo}</Text></Text>
                    <Text style={styles.subTitleBold}>Plus d'info: <Text style={styles.subTitle}>{props.route.params.props.more}</Text></Text>
                    <Text style={styles.subTitleBy}>{"À " + props.route.params.props.distance + "km de ma position"}</Text>
                </DefaultView>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        fontFamily: 'bold'
    },
    subTitle: {
        fontSize: 15,
        fontFamily: 'light',
        textAlign: 'justify'
    },
    subTitleMain: {
        fontSize: 15,
        fontFamily: 'regular',
        textAlign: 'justify',
        color: Colors.dark.tint
    },
    littleLogo: {
        height: 35,
        width: 35
    },
    bigLogo: {
        height: Layout.window.width * 0.3,
        width: Layout.window.width * 0.3,
        borderRadius: 10,
        marginRight: 15
    },
    bigbigLogo: {
        height: 125,
        width: Layout.window.width,
    },
    subTitleBold: {
        fontSize: 17,
        fontFamily: 'bold',
        paddingTop: 10
    },
    subTitleBy: {
        textAlign: 'right',
        fontSize: 14,
        fontFamily: 'light',
        paddingVertical: 20,
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
});
