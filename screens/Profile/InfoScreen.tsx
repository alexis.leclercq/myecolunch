import { Ionicons } from '@expo/vector-icons';
import React, { useEffect, useState } from 'react';
import { StyleSheet, Image, TouchableOpacity, Alert, Linking, ScrollView, Platform } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import ColoredButton from '../../components/ColoredButton';
import Input from '../../components/Input';

import { Text, View } from '../../components/Themed';
import Colors from '../../constants/Colors';
import firebase from '../../constants/firebase';
import Layout from '../../constants/Layout';

export default function InfoScreen(props: any) {
  const scheme = useColorScheme();


  return (
    <View style={{ height: '100%', width: '100%', paddingHorizontal: 20, paddingTop: 50 }}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
            <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => props.navigation.goBack()}>
                <Ionicons size={35} style={{ marginBottom: -3, marginRight: 10 }} name={Platform.OS === "android" ? "md-arrow-back" : "ios-arrow-back"} color={scheme === "dark" ? Colors.dark.text : Colors.light.text} />
                <Text style={styles.title}>Retour</Text>
            </TouchableOpacity>
            <Text style={styles.title}>Mes Points</Text>
        </View>
      <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ marginTop: 20 }}>
      <Text style={styles.subTitle}>Scans: 1 passage en boutique = 1 scan = 1 point</Text>
      <Text style={styles.subTitle}>- 20 points: un cookie fait maison 🍪</Text>
      <Text style={styles.subTitle}>- 40 points: une boisson chez l'un de nos partenaires 🍹</Text>
      <Text style={styles.subTitle}>- 60 points: -5 euros de réduction sur notre service de paniers fraîcheur 💸</Text>
      <Text style={styles.subTitle}>- 80 points: un plat chez l'un de nos partenaires 🥘</Text>
      <Text style={styles.subTitle}>- 100 points: un cadeau de la part de My Eco Lunch 🎁</Text>
      <Text style={styles.subTitle}>Vous pouvez choisir de conserver vos points pour atteindre les paliers suivants ou de les dépenser directement pour récupérer votre récompense. Lorsque vous souhaitez consommer vos points contactez @myecolunchlille sur Instagram</Text>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    fontFamily: 'bold',
  },
  littleTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    fontFamily: 'bold',
    marginBottom: 5,
  },
  littleLogo: {
    height: 35,
    width: 35
  },
  info: {
    fontSize: 16,
    fontFamily: 'light',
    marginTop: 5,
    marginBottom: 15,
  },
  signOut: {
    fontSize: 20,
    fontFamily: 'light',
    color: '#ff5454'
  },
  main: {
    fontSize: 20,
    fontFamily: 'light',
    color: '#ffcd4e',
    marginBottom: 10
  },
  desc: {
    marginTop: 30,
    fontSize: 14,
    fontFamily: 'light',
    textAlign: 'center'
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  confirmButton: {
    marginBottom: 20,
    marginTop: 5,
    width: Layout.window.width * 0.5,
  },
  subTitle: {
    fontSize: 17,
    fontFamily: 'light',
    paddingTop: 5,
    paddingBottom: 10
  },
  subTitleLess: {
    fontSize: 13,
    fontFamily: 'light',
    paddingTop: 5,
    paddingBottom: 10
  },
  subTitleMain: {
    fontSize: 20,
    fontFamily: 'bold',
    paddingTop: 5,
    paddingBottom: 10,
    color: Colors.dark.tint,
    paddingVertical: 5
  },
});
