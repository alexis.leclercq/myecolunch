import { Ionicons } from '@expo/vector-icons';
import React, { useEffect, useState } from 'react';
import { StyleSheet, Image, TouchableOpacity, Alert, Linking, ScrollView, Platform } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import ColoredButton from '../../components/ColoredButton';
import Input from '../../components/Input';

import { Text, View } from '../../components/Themed';
import Colors from '../../constants/Colors';
import firebase from '../../constants/firebase';
import Layout from '../../constants/Layout';

export default function ProfileScreen(props: any) {
  const scheme = useColorScheme();
  const [displayName, setDisplayName] = useState<string | any>(firebase.auth().currentUser?.displayName);
  const [firPass, setFirPass] = useState<string | any>("");
  const [secPass, setSecPass] = useState<string | any>("");
  const [points, setPoints] = useState<number>(0);
  const [user, setUser] = useState<any>(null);

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', async () => {
      let currentUser = firebase.auth().currentUser;

      await firebase.firestore().collection('users').doc(currentUser?.uid).get()
        .then((docRef) => {
          if (docRef.exists) {
            setUser(docRef.data());
            setPoints(docRef.data() && docRef.data()?.points ? docRef.data()?.points : 0)
          }
        })
    });
    return unsubscribe;
  }, [props.navigation])

  const changePass = () => {
    Alert.alert(
      "Changement de mot de passe",
      "Vous devrez vous reconnecter à votre prochaine session",
      [
        {
          text: "Annuler",
          onPress: () => null,
          style: "cancel"
        },
        {
          text: "OK", onPress: () => {
            if (firPass === secPass && firPass.length > 6)
              firebase.auth().currentUser?.updatePassword(firPass).then(() => {
                Alert.alert('Votre mot de passe a été modifié')
                setFirPass("");
                setSecPass("");
              }).catch(() => Alert.alert('', 'Vous devez vous être connecté récemment pour cela, déconnectez-vous et reconnectez-vous'));
            else
              Alert.alert("Mot de passe incorrect")
          }
        }
      ],
      { cancelable: true }
    );
  };

  return (
    <View style={{ height: '100%', width: '100%', paddingHorizontal: 20, paddingTop: 50 }}>
      <View style={{ flexDirection: 'row', marginBottom: 20 }}>
        <Text style={styles.title}>Mon compte</Text>
        <Image style={styles.littleLogo} source={require('../../assets/images/carot.png')} resizeMode={"contain"} />
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={styles.subTitle}>Vos points: <Text style={styles.title}>{points} </Text><TouchableOpacity onPress={() => props.navigation.navigate('InfoScreen')}><Ionicons size={30} style={{ marginBottom: -3 }} name={Platform.OS === "android" ? "md-information-circle-outline" : "ios-information-circle-outline"} color={Colors.dark.tint} /></TouchableOpacity></Text>
        <View>
          <View style={{ flexDirection: 'row', justifyContent: "space-between", alignItems: 'center' }}>
          <View/>
          <View style={{ alignItems: 'center' }}>
          <Text style={styles.littleTitle}>20</Text>
          <Text style={styles.littleTitle}>🍪</Text>
          </View>
          <View style={{ alignItems: 'center' }}>
          <Text style={styles.littleTitle}>40</Text>
          <Text style={styles.littleTitle}>🍹</Text>
          </View>
          <View style={{ alignItems: 'center' }}>
          <Text style={styles.littleTitle}>60</Text>
          <Text style={styles.littleTitle}>💸</Text>
          </View>
          <View style={{ alignItems: 'center' }}>
          <Text style={styles.littleTitle}>80</Text>
          <Text style={styles.littleTitle}>🥘</Text>
          </View>
          <View style={{ alignItems: 'center' }}>
          <Text style={styles.littleTitle}>100</Text>
          <Text style={styles.littleTitle}>🎁</Text>
          </View>
          </View>
          <View>
          <View style={{ height: 10, width: '100%', backgroundColor: scheme === "dark" ? Colors.dark.backgroundInverted : Colors.light.backgroundInverted, borderRadius: 20 }}/>
          <View style={{ position: 'absolute', top: 0, left: 0, height: 10, width: points > 100 ? '100%' : points + '%', backgroundColor: Colors.dark.tint, borderRadius: 20 }}/>
          </View>
        </View>
        <TouchableOpacity onPress={() => props.navigation.navigate('ScannerScreen', { user })} style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text style={styles.subTitleMain}>Scanner le code d'un partenaire</Text>
          <Image source={require('../../assets/images/photo.png')} resizeMode="contain" style={{ width: 25, height: 25, marginLeft: 10 }} />
        </TouchableOpacity>
        <Text style={styles.subTitleLess}>Gagnez des points en faisant des achats auprès de nos partenaires et obtenez des réductions chez ces derniers.</Text>
        <Text style={styles.littleTitle}>Nom Prénom</Text>
        <Input style={styles.info} placeholder={"Nom Prénom"} onChangeText={e => setDisplayName(e)} onSubmitEditing={async () => { firebase.auth().currentUser?.updateProfile({ displayName }); await firebase.firestore().collection('users').doc(firebase.auth().currentUser?.uid).update({ name: displayName }); }} value={displayName} dark={scheme === "dark"} />
        <Text style={styles.littleTitle}>E-mail</Text>
        <Text style={styles.info}>{firebase.auth().currentUser && firebase.auth().currentUser?.email ? firebase.auth().currentUser?.email : "Chargement..."}</Text>
        <Text style={styles.littleTitle}>Changer de mot de passe</Text>
        <Input style={styles.info} secureTextEntry placeholder={"Nouveau mot de passe"} onChangeText={e => setFirPass(e)} value={firPass} dark={scheme === "dark"} />
        <Input style={styles.info} secureTextEntry placeholder={"Confirmation du mot de passe"} onChangeText={e => setSecPass(e)} value={secPass} dark={scheme === "dark"} />
        <ColoredButton style={styles.confirmButton} text="Confirmer" onPress={changePass} />
        <TouchableOpacity onPress={() => firebase.auth().signOut()}>
          <Text style={styles.signOut}>Déconnexion</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => Linking.openURL('https://www.instagram.com/alexis.leclercq.737/')}>
          <Text style={styles.desc}>2020 © Alexis Leclercq</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    fontFamily: 'bold',
  },
  littleTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    fontFamily: 'bold',
    marginBottom: 5,
  },
  littleLogo: {
    height: 35,
    width: 35
  },
  info: {
    fontSize: 16,
    fontFamily: 'light',
    marginTop: 5,
    marginBottom: 15,
  },
  signOut: {
    fontSize: 20,
    fontFamily: 'light',
    color: '#ff5454'
  },
  main: {
    fontSize: 20,
    fontFamily: 'light',
    color: '#ffcd4e',
    marginBottom: 10
  },
  desc: {
    marginTop: 30,
    fontSize: 14,
    fontFamily: 'light',
    textAlign: 'center'
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  confirmButton: {
    marginBottom: 20,
    marginTop: 5,
    width: Layout.window.width * 0.5,
  },
  subTitle: {
    fontSize: 17,
    fontFamily: 'light',
    paddingTop: 5,
    paddingBottom: 10
  },
  subTitleLess: {
    fontSize: 13,
    fontFamily: 'light',
    paddingTop: 5,
    paddingBottom: 10
  },
  subTitleMain: {
    fontSize: 20,
    fontFamily: 'bold',
    paddingTop: 5,
    paddingBottom: 10,
    color: Colors.dark.tint,
    paddingVertical: 5
  },
});
