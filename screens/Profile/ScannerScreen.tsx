import React, { useState, useEffect } from 'react';
import { View as ThemedView, Text as ThemedText } from '../../components/Themed';
import { Text, View, StyleSheet, Alert } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import firebase from '../../constants/firebase';
import moment from 'moment';
import { useNavigation } from '@react-navigation/native';

export default function ScannerScreen(props: any) {
    const navigation = useNavigation();

    const [hasPermission, setHasPermission] = useState(false);
    const [scanned, setScanned] = useState(false);

    useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    const handleBarCodeScanned = async ({ type, data }: ({ type: string, data: string })) => {
        setScanned(true);
        if (data.startsWith('myecolunch://partner-')) {
            let currentUser = firebase.auth().currentUser;
            let date = moment().toISOString();

            if (!props.route.params.user.lastScan || moment().format('L') !== moment(props.route.params.user.lastScan).format('L')) {
                await firebase.firestore().collection('qrScanned').add({
                    partnerId: data.substr(21),
                    userId: currentUser?.uid,
                    date
                }).then(async () => {
                    await firebase.firestore().collection('users').doc(currentUser?.uid).update({
                        points: props.route.params.user && props.route.params.user.points ? (props.route.params.user.points + 1) : 1,
                        lastScan: date
                    }).then(() => {
                        if (props.route.params.user.points + 1 == 20 || props.route.params.user.points + 1 == 40 || props.route.params.user.points + 1 == 60 || props.route.params.user.points + 1 == 80 || props.route.params.user.points + 1 == 100)
                            Alert.alert('Bravo ! Nous te remercions pour ta fidélité !', 'Contacte-nous sur notre compte Instagram pour récupérer ton cadeau ✨')
                        else
                            Alert.alert('Merci pour votre fidélité ! 1 point a été ajouté à votre compte.');
                        navigation.goBack();
                    });
                });
            } else {
                Alert.alert('Merci de ne scanner qu\'un QR code par jour.')
            }
        } else
            Alert.alert('Code incorrect')
    };

    if (hasPermission === null) {
        return <Text>Nous avons besoin de l'autorisation d'accès à votre caméra pour scanner des codes barres.</Text>;
    }
    if (hasPermission === false) {
        return <Text>Pas d'accès à la caméra</Text>;
    }

    return (
        <View style={{ flex: 1 }}>
            <BarCodeScanner
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                style={StyleSheet.absoluteFillObject}
            />
            <ThemedView style={{ marginTop: 50, borderRadius: 25, paddingVertical: 10, paddingHorizontal: 30, marginHorizontal: 40 }}>
                <ThemedText style={{ fontFamily: 'light', textAlign: 'center' }}>Scannez le QR Code d'un partenaire</ThemedText>
            </ThemedView>
        </View>
    );
}