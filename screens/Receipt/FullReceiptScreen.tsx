import { Ionicons } from '@expo/vector-icons';
import React from 'react';
import { StyleSheet, Image, TouchableOpacity, Platform, View as DefaultView, ScrollView } from 'react-native';
import { useColorScheme } from 'react-native-appearance';

import { Text, View } from '../../components/Themed';
import Colors from '../../constants/Colors';
import { Video } from 'expo-av';
import Layout from '../../constants/Layout';

export default function FullReceiptScreen(props: any) {
    const scheme = useColorScheme();

    return (
        <View style={{ height: '100%', width: '100%', paddingTop: 50 }}>
            <View style={{ flexDirection: 'row', marginBottom: 20, justifyContent: 'space-between', marginHorizontal: 20 }}>
                <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => props.navigation.goBack()}>
                    <Ionicons size={35} style={{ marginBottom: -3 }} name={Platform.OS === "android" ? "md-arrow-back" : "ios-arrow-back"} color={scheme === "dark" ? Colors.dark.text : Colors.light.text} />
                </TouchableOpacity>
                <DefaultView style={{ flexDirection: 'row' }}>
                    <Text style={styles.title}>{props.route.params.props.name}</Text>
                    <Image style={styles.littleLogo} source={require('../../assets/images/carot.png')} resizeMode={"contain"} />
                </DefaultView>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
                {props.route.params.props.video && <Video
                    source={{ uri: props.route.params.props.video }}
                    rate={1.0}
                    volume={1.0}
                    isMuted={false}
                    resizeMode="contain"
                    shouldPlay
                    isLooping
                    useNativeControls
                    style={{ width: '100%', height: Layout.window.width / (16 / 9), marginBottom: 10 }}
                />}
                <View style={{ marginHorizontal: 20 }}>
                    <Text style={styles.subTitle}>{props.route.params.props.description}</Text>
                    <Text style={styles.subTitleBold}>{"Temps: " + props.route.params.props.time}</Text>
                    <Text style={styles.subTitleBold}>{"Difficulté: " + props.route.params.props.difficulty}</Text>
                    <Text style={[styles.title, { marginBottom: 10, marginTop: 30 }]}>Ingrédients</Text>
                    {props.route.params.props.ingredients && props.route.params.props.ingredients.map((item: Array<string>, index: number) => (
                        <Text key={index} style={styles.subTitle}>{item}</Text>
                    ))}
                    {props.route.params.props.preparation && props.route.params.props.preparation.length > 0 && <Text style={[styles.title, { marginBottom: 10, marginTop: 30 }]}>Préparation</Text>}
                    {props.route.params.props.preparation && props.route.params.props.preparation.map((item: Array<string>, index: number) => (
                        <Text key={index} style={styles.subTitle}>{item}</Text>
                    ))}
                    <Text style={styles.subTitleBy}>{"par " + props.route.params.props.by}</Text>
                </View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        fontFamily: 'bold'
    },
    subTitle: {
        fontSize: 15,
        fontFamily: 'light'
    },
    littleLogo: {
        height: 35,
        width: 35
    },
    subTitleBold: {
        fontSize: 17,
        fontFamily: 'bold',
        paddingTop: 10
    },
    subTitleBy: {
        textAlign: 'right',
        fontSize: 14,
        fontFamily: 'light',
        paddingVertical: 20,
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
});
