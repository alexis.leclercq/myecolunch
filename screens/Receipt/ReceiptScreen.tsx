import React, { useEffect, useState } from 'react';
import { StyleSheet, Image, FlatList, ActivityIndicator } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import CardReceipt from '../../components/CardReceipt';
import Input from '../../components/Input';
import SearchIcon from '../../components/SVG/SearchIcon';

import { Text, View } from '../../components/Themed';
import Colors from '../../constants/Colors';
import firebase from '../../constants/firebase';
import Layout from '../../constants/Layout';

export default function ReceiptScreen(props: any) {
  const scheme = useColorScheme();

  const [receipts, setReceipts] = useState<Array<any>>([]);
  const [isLoaded, setIsLoaded] = useState<boolean>(false);
  const [searchInput, setSearchInput] = useState<string>("");

  const entityRef = firebase.firestore().collection('receipts')

  useEffect(() => {
    entityRef.limit(10).get()
      .then((docRef) => {
        let receipts: Array<any> = [];
        for (let i in docRef.docs)
          receipts.push(docRef.docs[i].data());
        setReceipts(receipts);
        setIsLoaded(true);
      })
  }, []);

  if (isLoaded)
    return (
      <View style={{ height: '100%', width: '100%', paddingTop: 50 }}>
        <View style={{ marginHorizontal: 20, flexDirection: 'row' }}>
          <Text style={styles.title}>Les recettes</Text>
          <Image style={styles.littleLogo} source={require('../../assets/images/carot.png')} resizeMode={"contain"} />
        </View>
        <Input style={{ marginHorizontal: 20, marginTop: 10 }} value={searchInput} onChangeText={e => setSearchInput(e)} dark={scheme === "dark"} placeholder={"Chercher une recette"} endComponent={() => <SearchIcon color={scheme === "dark" ? Colors.dark.backgroundInverted : Colors.light.backgroundInverted} />} />
        <View>
        <FlatList horizontal showsHorizontalScrollIndicator={false} pagingEnabled data={receipts} keyExtractor={(item, index) => index.toString()}
          snapToInterval={Layout.window.width - (Layout.window.width * 0.2)}
          snapToAlignment={"start"}
          decelerationRate={0}
          centerContent={false}
          contentInset={{
            top: 0,
            left: 0,
            bottom: 0,
            right: Layout.window.width * 0.2
          }}
          renderItem={({ item }) => {
            if (item.name.toUpperCase().includes(searchInput.toUpperCase()))
              return <CardReceipt {...item} />;
            return <View />;
          }} />
          </View>
      </View>
    );
  else
    return <View style={{ height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
      <ActivityIndicator size={"large"} />
    </View>;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    fontFamily: 'bold'
  },
  littleLogo: {
    height: 35,
    width: 35
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
