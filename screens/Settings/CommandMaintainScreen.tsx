import React, { useEffect, useState } from 'react';
import { StyleSheet, Image, TouchableOpacity, FlatList, ActivityIndicator, Platform } from 'react-native';
import { useColorScheme } from 'react-native-appearance';
import Input from '../../components/Input';

import { Text, View } from '../../components/Themed';
import firebase from '../../constants/firebase';
import Colors from '../../constants/Colors';
import { ICommand } from '../../types';
import SearchIcon from '../../components/SVG/SearchIcon';
import { Ionicons } from '@expo/vector-icons';
import CommandElement from '../../components/CommandElement';


export default function CommandMaintainScreen(props: any) {
    const scheme = useColorScheme();

    const [user, setUser] = useState<any>(props.route.params.user);
    const [isLoaded, setIsLoaded] = useState<boolean>(false);
    const [searchInput, setSearchInput] = useState<string>("");

    const [elements, setElements] = useState<Array<ICommand>>([]);

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', async () => {
            if (!user || !user.administrator)
                props.navigation.navigate('SettingsScreen');
            await firebase.firestore().collection('commands').orderBy('date', "desc").where('visible', '==', true).get()
                .then(async (docRef) => {
                    if (docRef.size > 0) {
                        let tmpElements: Array<any> = [];
                        for (let i in docRef.docs) {
                            await firebase.firestore().collection('users').doc(docRef.docs[i].data().userId).get()
                                .then((docRef2) => {
                                    tmpElements.push({ ...docRef.docs[i].data(), id: docRef.docs[i].id, user: docRef2.data() });
                                })
                        }
                        setElements(tmpElements);
                        setIsLoaded(true);
                    }
                })
        });
        return unsubscribe;
    }, [props.navigation])

    return (
        <View style={{ height: '100%', width: '100%', paddingTop: 50, paddingHorizontal: 20 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => props.navigation.goBack()}>
                    <Ionicons size={35} style={{ marginBottom: -3, marginRight: 10 }} name={Platform.OS === "android" ? "md-arrow-back" : "ios-arrow-back"} color={scheme === "dark" ? Colors.dark.text : Colors.light.text} />
                    <Text style={styles.title}>Retour</Text>
                </TouchableOpacity>
                <Text style={styles.subTitleRed}>Administrateur</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
                <Text style={styles.title}>Gestion des commandes</Text>
                <Image style={styles.littleLogo} source={require('../../assets/images/carot.png')} resizeMode={"contain"} />
            </View>
            <Input style={{ marginBottom: 15 }} value={searchInput} onChangeText={e => setSearchInput(e)} dark={scheme === "dark"} placeholder={"Chercher par nom prénom"} endComponent={() => <SearchIcon color={scheme === "dark" ? Colors.dark.backgroundInverted : Colors.light.backgroundInverted} />} />
            {isLoaded ? <FlatList
                showsVerticalScrollIndicator={false}
                data={elements}
                keyExtractor={item => item.date.toString() + item.name.toString()}
                renderItem={({ item, index }) => {
                    if (item.name.toUpperCase().includes(searchInput.toUpperCase()))
                        return (
                            <CommandElement key={index} takeBag={item.takeBag} name={item.name} date={item.date} userId={item.userId} selected={item.selected} status={item.status} visible={item.visible} user={item.user} amount={item.amount} id={item.id} />
                        );
                    return <View />;
                }}
            /> : <ActivityIndicator size="large" />}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        fontFamily: 'bold',
        paddingBottom: 10
    },
    subTitle: {
        fontSize: 17,
        fontFamily: 'light',
        paddingTop: 5,
        paddingBottom: 10
    },
    subTitleRed: {
        fontSize: 17,
        fontFamily: 'light',
        paddingBottom: 10,
        color: '#ff5454'
    },
    subTitleMain: {
        fontSize: 20,
        fontFamily: 'bold',
        paddingTop: 5,
        paddingBottom: 10,
        color: Colors.dark.tint,
        paddingVertical: 5
    },
    littleLogo: {
        height: 35,
        width: 35
    },
    networkLogo: {
        height: 75,
        width: 75,
        margin: 10
    },
    networkLink: {
        textAlign: 'center',
        fontSize: 17,
        fontFamily: 'light',
        paddingLeft: 10,
        paddingTop: 5,
        paddingBottom: 10
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
});
