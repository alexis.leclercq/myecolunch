import React, { useEffect, useState } from 'react';
import { StyleSheet, Image, TouchableOpacity, ActivityIndicator, Platform, ScrollView, View as DefaultView } from 'react-native';
import { useColorScheme } from 'react-native-appearance';

import { Text, View } from '../../components/Themed';
import firebase from '../../constants/firebase';
import Colors from '../../constants/Colors';
import { ICommand, IUser, PartnersParamList } from '../../types';
import { Ionicons } from '@expo/vector-icons';
import ColoredButton from '../../components/ColoredButton';
import SearchIcon from '../../components/SVG/SearchIcon';
import Input from '../../components/Input';
import moment from 'moment';
import 'moment/locale/fr';

export default function ListUserScanScreen(props: any) {
    const scheme = useColorScheme();

    const [user, setUser] = useState<any>(props.route.params.user);
    const [isLoaded, setIsLoaded] = useState<boolean>(false);
    const [searchInput, setSearchInput] = useState<string>("");
    const [elements, setElements] = useState<Array<any>>(props.route.params.elements);

    return (
        <View style={{ height: '100%', width: '100%', paddingTop: 50, paddingHorizontal: 20 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => props.navigation.goBack()}>
                    <Ionicons size={35} style={{ marginBottom: -3, marginRight: 10 }} name={Platform.OS === "android" ? "md-arrow-back" : "ios-arrow-back"} color={scheme === "dark" ? Colors.dark.text : Colors.light.text} />
                    <Text style={styles.title}>Retour</Text>
                </TouchableOpacity>
                <Text style={styles.subTitleRed}>Administrateur</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
                <Text style={styles.title}>{props.route.params.owner.name}</Text>
                <Image style={styles.littleLogo} source={require('../../assets/images/carot.png')} resizeMode={"contain"} />
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
                {/* <Input value={searchInput} onChangeText={e => setSearchInput(e)} dark={scheme === "dark"} placeholder={"Chercher par partenaire"} endComponent={() => <SearchIcon color={scheme === "dark" ? Colors.dark.backgroundInverted : Colors.light.backgroundInverted} />} /> */}
                {elements.map((item, index) => {
                    console.log(item)
                    // if (item.name && item.name.toUpperCase().includes(searchInput.toUpperCase()))
                    return (
                        <DefaultView key={index} style={styles.block}>
                            <Text style={styles.subTitleBigWhite}>{item.partnerId}</Text>
                            <Text style={styles.subTitleWhite}>{moment(item.date).calendar()}</Text>
                        </DefaultView>
                    );
                    // return <View key={index} />;
                })}
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    block: {
        padding: 10,
        borderRadius: 10,
        backgroundColor: Colors.dark.tint,
        marginTop: 10
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        fontFamily: 'bold',
        paddingBottom: 10
    },
    titleSpaced: {
        fontSize: 24,
        fontWeight: 'bold',
        fontFamily: 'bold',
        paddingTop: 20
    },
    subTitle: {
        fontSize: 17,
        fontFamily: 'light',
        paddingTop: 5,
        paddingBottom: 10
    },
    subTitleWhite: {
        fontSize: 17,
        fontFamily: 'light',
        color: '#fff'
    },
    subTitleBigWhite: {
        fontSize: 17,
        fontFamily: 'bold',
        color: '#fff'
    },
    spacedButton: {
        marginTop: 10
    },
    subTitleRed: {
        fontSize: 17,
        fontFamily: 'light',
        paddingBottom: 10,
        color: '#ff5454'
    },
    subTitleMain: {
        fontSize: 20,
        fontFamily: 'bold',
        paddingTop: 5,
        paddingBottom: 10,
        color: Colors.dark.tint,
        paddingVertical: 5
    },
    littleLogo: {
        height: 35,
        width: 35
    },
    networkLogo: {
        height: 75,
        width: 75,
        margin: 10
    },
    networkLink: {
        textAlign: 'center',
        fontSize: 17,
        fontFamily: 'light',
        paddingLeft: 10,
        paddingTop: 5,
        paddingBottom: 10
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
});
