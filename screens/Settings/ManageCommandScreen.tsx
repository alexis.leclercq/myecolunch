import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity, Platform, ScrollView, Alert } from 'react-native';
import { useColorScheme } from 'react-native-appearance';

import { Text, View } from '../../components/Themed';
import firebase from '../../constants/firebase';
import Colors from '../../constants/Colors';
import { Ionicons } from '@expo/vector-icons';
import moment from 'moment';
import 'moment/locale/fr';
import CommandStatus from '../../constants/CommandStatus';
import CartElement from '../../components/CartElement';
import { IShop } from '../../types';
import ColoredButton from '../../components/ColoredButton';
import SimpleButton from '../../components/SimpleButton';
import { useNavigation } from '@react-navigation/native';


export default function ManageCommandScreen(props: any) {
    const scheme = useColorScheme();
    const navigation = useNavigation();

    const [elements, setElements] = useState<Array<IShop>>(props.route.params.props.selected);
    const [status, setStatus] = useState<"pending" | "sendAsked" | "sendWait" | "send" | "canceled">(props.route.params.props.status);

    const archiveCommand = () =>
        Alert.alert(
            "Archiver cette commande ?",
            "Cette commande deviendra invisible sur l'application",
            [
                {
                    text: "Annuler",
                    onPress: () => null,
                    style: "cancel"
                },
                { text: "Archiver", onPress: async () => await firebase.firestore().collection('commands').doc(props.route.params.props.id).update({ visible: false }).then((docRef) => navigation.goBack()), style: 'destructive' }
            ],
        );

    const sendPushNotification = (expoToken: string, status: string) => {
        const userExpoToken = expoToken
        let response = fetch('https://exp.host/--/api/v2/push/send', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                to: userExpoToken,
                sound: 'default',
                body: 'Mise à jour de votre commande: ' + CommandStatus[status].title,
                title: 'Votre commande'
            })
        })
    }

    const changeStatus = async (tmpStatus: "pending" | "sendAsked" | "sendWait" | "send" | "canceled") => {
        await firebase.firestore().collection('commands').doc(props.route.params.props.id).update({ status: tmpStatus })
            .then(() => {
                setStatus(tmpStatus)
                sendPushNotification(props.route.params.props.user.expoToken, tmpStatus);
            })
    }

    return (
        <View style={{ height: '100%', width: '100%', paddingTop: 50 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 20 }}>
                <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => props.navigation.goBack()}>
                    <Ionicons size={35} style={{ marginBottom: -3, marginRight: 10 }} name={Platform.OS === "android" ? "md-arrow-back" : "ios-arrow-back"} color={scheme === "dark" ? Colors.dark.text : Colors.light.text} />
                    <Text style={styles.title}>Retour</Text>
                </TouchableOpacity>
                <Text style={styles.subTitleRed}>Administrateur</Text>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ marginHorizontal: 20 }}>
                    <Text style={styles.title}>{"Commande de " + props.route.params.props.name}</Text>
                    <Text style={styles.subTitle}>{"Montant: " + Number((props.route.params.props.amount).toFixed(2)) + "€"}</Text>
                    <Text style={styles.subTitle}>Status: <Text style={styles.subTitleBold}>{CommandStatus[status].title}</Text></Text>
                    <Text style={styles.subTitle}>Sac: <Text style={styles.subTitleBold}>{props.route.params.props.takeBag ? "Oui" : "Non"}</Text></Text>
                    <Text style={styles.subTitle}>{"Date de commande: " + moment(props.route.params.props.date).calendar()}</Text>
                    {elements.map((item, index) => (
                        <CartElement key={index} name={item.name} price={item.price} quantity={item.quantity} number={item.number} setShop={false} canEdit={false} />
                    ))}
                </View>
                {status !== "pending" && <SimpleButton text={"Définir \"En attente\""} onPress={() => changeStatus("pending")} style={{ marginTop: 20, marginHorizontal: 20 }} />}
                {status !== "sendAsked" && <SimpleButton text={"Définir \"Attente de stock\""} onPress={() => changeStatus("sendAsked")} style={{ marginTop: 20, marginHorizontal: 20 }} />}
                {status !== "sendWait" && <SimpleButton text={"Définir \"Prête\""} onPress={() => changeStatus("sendWait")} style={{ marginTop: 20, marginHorizontal: 20 }} />}
                {status !== "send" && <ColoredButton text={"Définir \"Terminée\""} onPress={() => changeStatus("send")} style={{ marginTop: 20, marginHorizontal: 20 }} />}
                <ColoredButton text={"Archiver cette commande"} onPress={archiveCommand} style={{ backgroundColor: '#ff5454', marginTop: 50, marginBottom: 20, marginHorizontal: 20 }} />
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        fontFamily: 'bold',
        paddingBottom: 10
    },
    subTitle: {
        fontSize: 17,
        fontFamily: 'light',
        paddingTop: 5,
        paddingBottom: 10
    },
    subTitleBold: {
        fontSize: 17,
        fontFamily: 'bold',
        paddingTop: 5,
        paddingBottom: 10
    },
    subTitleRed: {
        fontSize: 17,
        fontFamily: 'light',
        paddingBottom: 10,
        color: '#ff5454'
    },
    subTitleMain: {
        fontSize: 20,
        fontFamily: 'bold',
        paddingTop: 5,
        paddingBottom: 10,
        color: Colors.dark.tint,
        paddingVertical: 5
    },
    littleLogo: {
        height: 35,
        width: 35
    },
    networkLogo: {
        height: 75,
        width: 75,
        margin: 10
    },
    networkLink: {
        textAlign: 'center',
        fontSize: 17,
        fontFamily: 'light',
        paddingLeft: 10,
        paddingTop: 5,
        paddingBottom: 10
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
});
