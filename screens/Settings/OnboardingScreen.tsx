import React, { useEffect, useState } from 'react';
import { StyleSheet, Image, TouchableOpacity, Linking, ScrollView, Switch, View as DefaultView, Platform } from 'react-native';

import { Text, View } from '../../components/Themed';
import firebase from '../../constants/firebase';
import Colors from '../../constants/Colors';
import { Ionicons } from '@expo/vector-icons';
import { useColorScheme } from 'react-native-appearance';
import Layout from '../../constants/Layout';

export default function OnboardingScreen(props: any) {
  const scheme = useColorScheme();

  return (
    <View style={{ paddingTop: 50, paddingHorizontal: 20 }}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
        <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => props.navigation.goBack()}>
          <Ionicons size={35} style={{ marginBottom: -3, marginRight: 10 }} name={Platform.OS === "android" ? "md-arrow-back" : "ios-arrow-back"} color={scheme === "dark" ? Colors.dark.text : Colors.light.text} />
          <Text style={styles.title}>Retour</Text>
        </TouchableOpacity>
      </View>
      <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{height: '100%', width: '100%'}}>
      <Text style={styles.header}>Qui sommes-nous ?</Text>
      <View style={styles.row}>
        <Image style={styles.networkLogo} source={require('../../assets/images/on1.png')} resizeMode={"contain"} />
        <View style={styles.center}>
          <Text style={styles.subTitle}>Une association</Text>
          <Text style={styles.subTitleLess}>6 étudiants en communication à l'ISCOM Lille, motivés à VOUS aider !</Text>
        </View>
      </View>
      <View style={styles.row}>
        <Image style={styles.networkLogo} source={require('../../assets/images/on2.png')} resizeMode={"contain"} />
        <View style={styles.center}>
          <Text style={styles.subTitle}>Mangez</Text>
          <Text style={styles.subTitleLess}>De savoureux repas grâce à notre carte interactive qui recense les meilleures adresses Lilloises.</Text>
        </View>
      </View>
      <View style={styles.row}>
        <Image style={styles.networkLogo} source={require('../../assets/images/on3.png')} resizeMode={"contain"} />
        <View style={styles.center}>
          <Text style={styles.subTitle}>Économisez</Text>
          <Text style={styles.subTitleLess}>Grâce à l'application My Eco Lunch, bénéficiez de réductions chez nos partenaires.</Text>
        </View>
      </View>
      <View style={styles.row}>
        <Image style={styles.networkLogo} source={require('../../assets/images/on4.png')} resizeMode={"contain"} />
        <View style={styles.center}>
          <Text style={styles.subTitle}>Cuisinez</Text>
          <Text style={styles.subTitleLess}>De délicieuses recettes triées sur le volet par notre équipe. Suivez les tutoriels vidéo de notre équipe via l'application !</Text>
        </View>
      </View>
      <View style={styles.row}>
        <Image style={styles.networkLogo} source={require('../../assets/images/on5.png')} resizeMode={"contain"} />
        <View style={styles.center}>
          <Text style={styles.subTitle}>Simplifiez-vous la vie !</Text>
          <Text style={styles.subTitleLess}>Notre service de paniers fraîcheurs vous simplifiera la vie. En un clic, vous pourrez commander et récupérer vos produits frais au campus.</Text>
        </View>
      </View>
      </ScrollView>
    </View >
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    fontFamily: 'bold',
    paddingBottom: 20
  },
  header: {
    fontSize: 24,
    fontWeight: 'bold',
    fontFamily: 'bold',
    paddingBottom: 40,
    paddingTop: 10,
    textAlign: 'center'
  },
  row: {
    flexDirection: 'row',
  },
  center: {
    justifyContent: 'center',
    flex: 1
  },
  subTitle: {
    fontSize: 17,
    fontFamily: 'bold',
  },
  subTitleLess: {
    fontSize: 13,
    fontFamily: 'light',
    paddingTop: 5,
  },
  networkLogo: {
    height: Layout.window.width * 0.2,
    width: Layout.window.width * 0.2,
    margin: 10
  }
});
