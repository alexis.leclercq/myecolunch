import React, { useEffect, useState } from 'react';
import { StyleSheet, Image, TouchableOpacity, Linking, ScrollView, Switch, View as DefaultView, Platform } from 'react-native';
import Separator from '../../components/Separator';

import { Text, View } from '../../components/Themed';
import firebase from '../../constants/firebase';
import Colors from '../../constants/Colors';
import { Ionicons } from '@expo/vector-icons';

export default function SettingsScreen(props: any) {
  const [user, setUser] = useState<any>(null);
  const [openNotifs, setOpenNotifs] = useState<boolean>(false);
  const [points, setPoints] = useState<number>(0);

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', async () => {
      let currentUser = firebase.auth().currentUser;

      await firebase.firestore().collection('users').doc(currentUser?.uid).get()
        .then((docRef) => {
          if (docRef.exists) {
            setUser(docRef.data());
            setOpenNotifs(docRef.data() && docRef.data()?.notifsAdmin ? docRef.data()?.notifsAdmin : false)
            setPoints(docRef.data() && docRef.data()?.points ? docRef.data()?.points : 0)
          }
        })
    });
    return unsubscribe;
  }, [props.navigation])

  const handleOpenNotifs = async () => {
    let newNotifs = !openNotifs;
    setOpenNotifs(newNotifs);
    await firebase.firestore().collection('users').doc(firebase.auth().currentUser?.uid).update({ notifsAdmin: newNotifs });
  }

  return (
    <View style={{ height: '100%', width: '100%', paddingTop: 50, paddingHorizontal: 20 }}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
      <View style={{ flexDirection: 'row' }}>
        <Text style={styles.title}>Paramètres</Text>
        <Image style={styles.littleLogo} source={require('../../assets/images/carot.png')} resizeMode={"contain"} />
        </View>
         <TouchableOpacity onPress={() => props.navigation.navigate('OnboardingScreen')}><Ionicons size={30} style={{ marginBottom: -3 }} name={Platform.OS === "android" ? "md-information-circle-outline" : "ios-information-circle-outline"} color={Colors.dark.tint} /></TouchableOpacity>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        {user && user.administrator && <View>
          <Separator />
          <Text style={styles.subTitleRed}>Administrateur</Text>
          <Text style={styles.subTitleLess}>Cette partie est réservée aux administrateurs.</Text>
          <TouchableOpacity onPress={() => props.navigation.navigate('CommandMaintainScreen', { user })}>
            <Text style={styles.subTitleMain}>Gérer les commandes →</Text>
          </TouchableOpacity>
          <DefaultView style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image source={require('../../assets/images/notifications.png')} resizeMode="contain" style={styles.image} />
            <Text style={[styles.subTitle, { width: '70%' }]}>Notifications</Text>
            <Switch
              trackColor={{ false: Colors.light.tint, true: Colors.light.tint }}
              thumbColor={openNotifs ? "#fff" : Colors.light.tint}
              ios_backgroundColor="#fff"
              onValueChange={handleOpenNotifs}
              value={openNotifs}
            />
          </DefaultView>
          <Text style={styles.subTitleLess}>Ces notifications servent à être informé de chaque nouvelle commande passée.</Text>
          <Text style={styles.subTitleLess}>Les statistiques partenaires vont vous aider à visualiser chaque scan de code barre. Vous pouvez aussi voir quels sont les utilisateurs ayant au moins 10 points.</Text>
          <TouchableOpacity onPress={() => props.navigation.navigate('StatsPartnerScreen', { user })}>
            <Text style={styles.subTitleMain}>Statistiques partenaires →</Text>
          </TouchableOpacity>
          <Separator />
        </View>}
        <Text style={styles.subTitle}>Conditions de vente:</Text>
        <Text style={styles.subTitle}>- La commande de paniers fraîcheur résulte d'un procédé de bon sens. Si une commande se voit impayée, l'utilisateur sera banni de l'application.</Text>
        <Text style={styles.subTitle}>- Toute usurpation d'identité via l'adresse e-mail entrainera une suppresion du compte</Text>
        <Text style={styles.subTitle}>- Les paiements s'effectuent via l'application Pumpkin au numéro de téléphone suivant: 06 52 51 31 29</Text>
        <Text style={styles.subTitle}>- Si vous commandez un panier fraîcheur et ne sélectionnez pas l'achat de sac en kraft, il faut impérativement amener votre sac. Autrement, un sac en kraft vous sera facturé</Text>
        <Text style={styles.subTitle}>- Toute tentative d'abus du système de fidélité qui sera répertoriée peut entrainer un bannissement de l'application en cas de non-présentation de justificatif d'achat.</Text>
        <Text style={styles.networkLink}>Rejoignez-nous sur nos réseaux sociaux !</Text>
        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity onPress={() => Linking.openURL('https://www.instagram.com/myecolunchlille/')}>
            <Image style={styles.networkLogo} source={require('../../assets/images/instagram.png')} resizeMode={"contain"} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => Linking.openURL('https://www.facebook.com/myecolunch/')}>
            <Image style={styles.networkLogo} source={require('../../assets/images/facebook.png')} resizeMode={"contain"} />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View >
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    fontFamily: 'bold',
    paddingBottom: 20
  },
  subTitle: {
    fontSize: 17,
    fontFamily: 'light',
    paddingTop: 5,
    paddingBottom: 10
  },
  subTitleLess: {
    fontSize: 13,
    fontFamily: 'light',
    paddingTop: 5,
    paddingBottom: 10
  },
  subTitleRed: {
    fontSize: 17,
    fontFamily: 'light',
    paddingTop: 5,
    paddingBottom: 10,
    color: '#ff5454'
  },
  subTitleMain: {
    fontSize: 20,
    fontFamily: 'bold',
    paddingTop: 5,
    paddingBottom: 10,
    color: Colors.dark.tint,
    paddingVertical: 5
  },
  littleLogo: {
    height: 35,
    width: 35
  },
  networkLogo: {
    height: 50,
    width: 50,
    margin: 10
  },
  networkLink: {
    textAlign: 'center',
    fontSize: 17,
    fontFamily: 'light',
    paddingLeft: 10,
    paddingTop: 5,
    paddingBottom: 10
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  image: {
    height: 25,
    width: 25,
    marginRight: 10
  },
});
