import React, { useEffect, useState } from 'react';
import { StyleSheet, Image, TouchableOpacity, ActivityIndicator, Platform, ScrollView, View as DefaultView } from 'react-native';
import { useColorScheme } from 'react-native-appearance';

import { Text, View } from '../../components/Themed';
import firebase from '../../constants/firebase';
import Colors from '../../constants/Colors';
import { ICommand, IUser, PartnersParamList } from '../../types';
import { Ionicons } from '@expo/vector-icons';
import ColoredButton from '../../components/ColoredButton';
import SearchIcon from '../../components/SVG/SearchIcon';
import Input from '../../components/Input';

export default function StatsByPartnerScreen(props: any) {
    const scheme = useColorScheme();

    const [user, setUser] = useState<any>(props.route.params.user);
    const [isLoaded, setIsLoaded] = useState<boolean>(false);
    const [searchInput, setSearchInput] = useState<string>("");
    const [elements, setElements] = useState<Array<any>>([]);

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', async () => {
            if (!user || !user.administrator)
                props.navigation.navigate('SettingsScreen');
            await firebase.firestore().collection('partners').get()
                .then(async (docRef) => {
                    if (docRef.size > 0) {
                        let tmpElements: Array<any> = [];
                        for (let i in docRef.docs) {
                            await firebase.firestore().collection('qrScanned').where('partnerId', '==', docRef.docs[i].id).orderBy('date', 'desc').get().then((docRef2) => {
                                let tmpScanned: Array<any> = [];
                                for (let j in docRef2.docs)
                                    tmpScanned.push(docRef2.docs[j].data());
                                tmpElements.push({ ...docRef.docs[i].data(), id: docRef.docs[i].id, qrScanned: tmpScanned });
                            })
                        }
                        setElements(tmpElements);
                        setIsLoaded(true);
                    }
                })
        });
        return unsubscribe;
    }, [props.navigation])

    return (
        <View style={{ height: '100%', width: '100%', paddingTop: 50, paddingHorizontal: 20 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity style={{ flexDirection: 'row' }} onPress={() => props.navigation.goBack()}>
                    <Ionicons size={35} style={{ marginBottom: -3, marginRight: 10 }} name={Platform.OS === "android" ? "md-arrow-back" : "ios-arrow-back"} color={scheme === "dark" ? Colors.dark.text : Colors.light.text} />
                    <Text style={styles.title}>Retour</Text>
                </TouchableOpacity>
                <Text style={styles.subTitleRed}>Administrateur</Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
                <Text style={styles.title}>Statistiques par partenaires</Text>
                <Image style={styles.littleLogo} source={require('../../assets/images/carot.png')} resizeMode={"contain"} />
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
                <Input value={searchInput} onChangeText={e => setSearchInput(e)} dark={scheme === "dark"} placeholder={"Chercher par partenaire"} endComponent={() => <SearchIcon color={scheme === "dark" ? Colors.dark.backgroundInverted : Colors.light.backgroundInverted} />} />
                {isLoaded ? elements.map((item, index) => {
                    if (item.name && item.name.toUpperCase().includes(searchInput.toUpperCase()))
                        return (
                            <TouchableOpacity onPress={() => props.navigation.navigate('ListPartnerScanScreen', { user, elements: item.qrScanned, owner: item })} key={index} style={styles.block}>
                                <DefaultView style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={styles.subTitleBigWhite}>{item.name}</Text>
                                    <Text style={styles.subTitleWhite}>{item.qrScanned.length + " scan"}</Text>
                                </DefaultView>
                            </TouchableOpacity>
                        );
                    return <View key={index} />;
                }) : <ActivityIndicator style={{ marginTop: 20 }} size="large" />}
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    block: {
        padding: 10,
        borderRadius: 10,
        backgroundColor: Colors.dark.tint,
        marginTop: 10
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        fontFamily: 'bold',
        paddingBottom: 10
    },
    titleSpaced: {
        fontSize: 24,
        fontWeight: 'bold',
        fontFamily: 'bold',
        paddingTop: 20
    },
    subTitle: {
        fontSize: 17,
        fontFamily: 'light',
        paddingTop: 5,
        paddingBottom: 10
    },
    subTitleWhite: {
        fontSize: 17,
        fontFamily: 'light',
        color: '#fff'
    },
    subTitleBigWhite: {
        fontSize: 17,
        fontFamily: 'bold',
        color: '#fff'
    },
    spacedButton: {
        marginTop: 10
    },
    subTitleRed: {
        fontSize: 17,
        fontFamily: 'light',
        paddingBottom: 10,
        color: '#ff5454'
    },
    subTitleMain: {
        fontSize: 20,
        fontFamily: 'bold',
        paddingTop: 5,
        paddingBottom: 10,
        color: Colors.dark.tint,
        paddingVertical: 5
    },
    littleLogo: {
        height: 35,
        width: 35
    },
    networkLogo: {
        height: 75,
        width: 75,
        margin: 10
    },
    networkLink: {
        textAlign: 'center',
        fontSize: 17,
        fontFamily: 'light',
        paddingLeft: 10,
        paddingTop: 5,
        paddingBottom: 10
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
});
