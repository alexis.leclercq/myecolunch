export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  MapScreen: undefined;
  CartScreen: undefined;
  ReceiptScreen: undefined;
  ProfileScreen: undefined;
  SettingsScreen: undefined;
};

export type NotFoundParamList = {
  NotFoundScreen: undefined;
};

export type LoginParamList = {
  First: undefined;
  Register: undefined;
  LoginScreen: undefined;
  ResetPassword: undefined;
};

export type LoadingParamList = {
  Loading: undefined;
  Login: undefined;
  Main: undefined;
  Ask: undefined;
};

export type MapParamList = {
  MapScreen: undefined;
  PartnerScreen: undefined;
};

export type CartParamList = {
  CartScreen: undefined;
  CommandScreen: undefined;
};

export type ReceiptParamList = {
  ReceiptScreen: undefined;
  FullReceiptScreen: undefined;
};

export type ProfileParamList = {
  ProfileEditScreen: undefined;
  ScannerScreen: undefined;
  InfoScreen: undefined;
};

export type SettingsParamList = {
  SettingsScreen: undefined;
  CommandMaintainScreen: undefined;
  ManageCommandScreen: undefined;
  StatsPartnerScreen: undefined;
  StatsByPartnerScreen: undefined;
  StatsByUserScreen: undefined;
  ListPartnerScanScreen: undefined;
  ListUserScanScreen: undefined;
  OnboardingScreen: undefined;
};

export type PartnersParamList = {
  name: string;
  description: string;
  coordinate: {
    latitude: number,
    longitude: number
  },
  address: string,
  url: string,
  urlView: string,
  promo: string,
  hours: string,
  more: string,
  distance?: string
};

export type ICart = {
  name: string;
  id: string;
  price: number;
  quantity: string;
}

export type IShop = {
  name: string;
  quantity: string;
  number: number;
  price: number;
};

export type IUser = {
  expoToken?: string;
  last_login: string;
  points?: number;
  administrator?: boolean;
  notifsAdmin?: boolean;
  lastScan?: string;
  name?: string;
  email?: string;
};

export type ICommand = {
  userId: string;
  selected: Array<IShop>;
  status: string;
  date: string;
  amount: number;
  name: string;
  user: IUser;
  visible: true;
  id?: string;
  takeBag: boolean;
};